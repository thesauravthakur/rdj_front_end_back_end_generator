import React, { Fragment } from 'react';

import clsx from 'clsx';
import { Link } from 'react-router-dom';

import { IconButton, Box } from '@material-ui/core';

import projectLogo from '../../../assets/images/nepal-logo.png';

const HeaderLogo = (props) => {

  return (
    <Fragment>
      <div className={clsx('app-header-logo', {})}>
        <Link to="/admin/DashboardDefault" className="header-logo-wrapper-link">
          <Box
            className="header-logo-wrapper"

            title="Library Management System">
            <IconButton
              color="primary"
              size="medium"
              className="header-logo-wrapper-btn">
              <img
                className="app-header-logo-img"
                alt=" React Admin Dashboard with Material-UI Free"
                src={projectLogo}
              />

            </IconButton>

            <Box className="header-logo-text">
              <div style={{ fontSize: '20px' }}>{props.setting ? props.setting.name : ''}</div>
              <div style={{ fontSize: '15px', marginTop: -5 }}>{props.setting ? props.setting.address : ''}</div>
              <div style={{ fontSize: '15px', marginTop: -5 }}>{props.setting ? props.setting.phone_number : ''}</div>
            </Box>
          </Box>


          <div class="bird-container bird-container--one">
            <div class="bird bird--one"></div>
          </div>

          <div class="bird-container bird-container--two">
            <div class="bird bird--two"></div>
          </div>

          <div class="bird-container bird-container--three">
            <div class="bird bird--three"></div>
          </div>

          <div class="bird-container bird-container--four">
            <div class="bird bird--four"></div>
          </div>
        </Link>
      </div>
      <div style={{ width: '650px' }}></div>


    </Fragment>
  );
};

export default HeaderLogo;
