import React, { lazy, Suspense } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { AnimatePresence, motion } from 'framer-motion';
import * as actions from '../store/actions/auth'
import { connect } from "react-redux";
import { ThemeProvider } from '@material-ui/styles';
import saurav from '../components/saurav/index'
import MuiTheme from '../theme';


// Layout Blueprints

import { LeftSidebar, PresentationLayout } from '../main';

// Example Pages

import Setting from '../pages/Admin/Setting';
import RouteMiddleware from './middleware';
import Loading from '../components/Loading/index';
import WelcomePage from "../components/WelcomePage";
import SliderForm from '../pages/Admin/Slider/form';
import SliderIndex from '../pages/Admin/Slider/index';
import ContactIndex from '../pages/Admin/Contact/index';

const DashboardDefault = lazy(() => import('../pages/Admin/Dashboard'));
const LogInPage = lazy(() => import('../pages/Admin/LoginPage'));
// const SignUp = lazy(() => import('../pages/Admin/SignupPage'));




const Routes = (props) => {
  const location = useLocation();
  console.log('props', props)

  const pageVariants = {
    initial: {
      opacity: 0,
      scale: 0.99
    },
    in: {
      opacity: 1,
      scale: 1
    },
    out: {
      opacity: 0,
      scale: 1.01
    }
  };

  const pageTransition = {
    type: 'tween',
    ease: 'anticipate',
    duration: 0.4
  };

    console.log(props)
  return (
    <ThemeProvider theme={MuiTheme}>
      <AnimatePresence>
        <Suspense
          fallback={
            <Loading />
          }>
          <Switch>
             {/* <Redirect exact from="/admin" to="/admin/DashboardDefault" />
             <Redirect exact from="/login" to="/admin/DashboardDefault" />
             <Redirect exact from="/admin" to="/admin/login" /> */}

             {props.isAuthenticated &&<Redirect exact from="/admin/login" to="/admin/DashboardDefault" />}
             {props.isAuthenticated &&<Redirect exact from="/admin" to="/admin/DashboardDefault" />}
             {/* {!props.isAuthenticated &&<Redirect exact from="/admin/DashboardDefault" to="/admin/login" />} */}

            {/* {!props.isAuthenticated ? */}
            <Route path={[
              '/admin/login',
              '/admin/auth/signup',
              '/admin/saurav',
              '/admin/saurav',
            ]}>

              <PresentationLayout>
                <Switch location={location} key={location.pathname}>
                  <motion.div
                    initial="initial"
                    animate="in"
                    exit="out"
                    variants={pageVariants}
                    transition={pageTransition}>
                    <Route path="/admin/login" component={LogInPage} />
                    <Route path="/admin/saurav" component={saurav} />
                  </motion.div>
                </Switch>
              </PresentationLayout>
            </Route>


            {props.isAuthenticated ?
              <Route
                path={[
                  '/admin/DashboardDefault',
                  '/admin/Setting',
                  "/admin/contact",
                  "/admin/Slider",
                  '/admin/Slider-create',
                  '/admin/Slider-edit/:id',
                ]}>
                <LeftSidebar>
                  <Switch location={location} key={location.pathname}>
                    <motion.div
                      initial="initial"
                      animate="in"
                      exit="out"
                      variants={pageVariants}
                      transition={pageTransition}>
                      <RouteMiddleware
                        name='dashboard-list'
                        exact
                        path="/admin/DashboardDefault"
                        render={() => <DashboardDefault />}
                      />
                      <RouteMiddleware
                        path="/admin/Setting"
                        name="setting-edit"
                        component={Setting} />
                      <RouteMiddleware
                        path="/admin/contact"
                        name="setting-edit"
                        component={ContactIndex} />
                      <RouteMiddleware
                        path="/admin/Slider"
                        name="setting-edit"
                        component={SliderIndex} />
                      <RouteMiddleware
                        path='/admin/Slider-create'
                        name="setting-edit"
                        component={SliderForm} />
                      <RouteMiddleware
                        path='/admin/Slider-edit/:id'
                        name="setting-edit"
                        component={SliderForm} />

                    </motion.div>
                  </Switch>
                </LeftSidebar>
              </Route>
              :
              <Redirect exact to="/admin/login" />
            }

          </Switch>
        </Suspense>
      </AnimatePresence>
    </ThemeProvider>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    token:state.auth.token,
    permissions: (typeof state.auth['user'] != 'undefined' && typeof state.auth.user['permissions'] != 'undefined') ? state.auth.user.permissions : []
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routes);