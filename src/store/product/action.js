import axios from "axios";
import * as actionTypes from "./actionType";
import { mainLink } from "../utils";



export const productPostStart= () => {
	return {
		type: actionTypes.PRODUCT_POST_START
		};
};


export const productPostSuccess= (productDetail) => {
	return {
		type: actionTypes.PRODUCT_POST_SUCCESS,
		 productDetail: productDetail
		};
};


export const productPostFail= (error) => {
	return {
		type: actionTypes.PRODUCT_POST_FAIL,
		 error: error
		};
};









export const productGetStart= () => {
	return {
		type: actionTypes.PRODUCT_GET_START
		};
};


export const productGetSuccess= (product) => {
	return {
		type: actionTypes.PRODUCT_GET_SUCCESS,
		 product: product
		};
};


export const productGetFail= (error) => {
	return {
		type: actionTypes.PRODUCT_GET_FAIL,
		 error: error
		};
};









export const productEditStart= () => {
	return {
		type: actionTypes.PRODUCT_EDIT_START
		};
};


export const productEditSuccess= (productDetail) => {
	return {
		type: actionTypes.PRODUCT_EDIT_SUCCESS,
		 productDetail: productDetail
		};
};


export const productEditFail= (error) => {
	return {
		type: actionTypes.PRODUCT_EDIT_FAIL,
		 error: error
		};
};









export const productDeleteStart= () => {
	return {
		type: actionTypes.PRODUCT_DELETE_START
		};
};


export const productDeleteSuccess= (productDetail) => {
	return {
		type: actionTypes.PRODUCT_DELETE_SUCCESS,
		 productDetail: productDetail
		};
};


export const productDeleteFail= (error) => {
	return {
		type: actionTypes.PRODUCT_DELETE_FAIL,
		 error: error
		};
};









export const productDetailStart= () => {
	return {
		type: actionTypes.PRODUCT_DETAIL_START
		};
};


export const productDetailSuccess= (productDetail) => {
	return {
		type: actionTypes.PRODUCT_DETAIL_SUCCESS,
		 productDetail: productDetail
		};
};


export const productDetailFail= (error) => {
	return {
		type: actionTypes.PRODUCT_DETAIL_FAIL,
		 error: error
		};
};









export const postproduct = (product,token, onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(productPostStart());
		axios.defaults.headers = {
			 "Content-Type": "application/json",
			 Authorization: `Token ${token}`,
		};
		axios.
			 .post(mainLink + `/api/v1/product/`,product) 
			 .then(res => {
				 dispatch(productPostSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess()
				}
			}
			 ).catch(err => {
				 dispatch(productPostFail(err));
				 if (typeof onError == 'function') {
					onError()
				}
			 });
	 };
 };









export const getproduct = (token, onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(productGetStart());
		axios.defaults.headers = {
			 "Content-Type": "application/json",
			 Authorization: `Token ${token}`,
		};
		axios.
			 .get(mainLink + `/api/v1/product/`) 
			 .then(res => {
				 dispatch(productGetSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess()
				}
			}
			 ).catch(err => {
				 dispatch(productGetFail(err));
				 if (typeof onError == 'function') {
					onError()
				}
			 });
	 };
 };









export const editproduct = (product,token,id, onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(productEditStart());
		axios.defaults.headers = {
			 "Content-Type": "application/json",
			 Authorization: `Token ${token}`,
		};
		axios.
			 .put(mainLink + `/api/v1/product/${id}/`,product) 
			 .then(res => {
				 dispatch(productEditSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess()
				}
			}
			 ).catch(err => {
				 dispatch(productEditFail(err));
				 if (typeof onError == 'function') {
					onError()
				}
			 });
	 };
 };









export const getDetailproduct = (token,id, onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(productDetailStart());
		axios.defaults.headers = {
			 "Content-Type": "application/json",
			 Authorization: `Token ${token}`,
		};
		axios.
			 .get(mainLink + `/api/v1/product/${id}/`) 
			 .then(res => {
				 dispatch(productDetailSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess()
				}
			}
			 ).catch(err => {
				 dispatch(productDetailFail(err));
				 if (typeof onError == 'function') {
					onError()
				}
			 });
	 };
 };









export const deleteproduct = (token,id, onSuccess = null, onError = null) => {
	return dispatch => {
		dispatch(productDeleteStart());
		axios.defaults.headers = {
			 "Content-Type": "application/json",
			 Authorization: `Token ${token}`,
		};
		axios.
			 .delete(mainLink + `/api/v1/product/${id}/`) 
			 .then(res => {
				 dispatch(productDeleteSuccess(res.data));
				 if (typeof onSuccess == 'function') {
					onSuccess()
				}
			}
			 ).catch(err => {
				 dispatch(productDeleteFail(err));
				 if (typeof onError == 'function') {
					onError()
				}
			 });
	 };
 };









