import axios from "axios";
import * as actionTypes from "./actionTypes";
import { getBookAuthor, getBookPublication, getBook, getBookType, getComponent, getBookEdition, getBookRack, getBookRoom, getBookSubCategory } from "./book";
import { getmember, getmemberType, getpermission } from './member'
import { mainLink } from '../utils';
import axiosInstance from '../../axios'


export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = user => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    user
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};



export const checkAuthTimeout = expirationTime => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout());
    }, expirationTime * 1000);
  };
};

export const updateTrainingInfo = data => {
  console.log(data)
  return {
    type: actionTypes.UPDATE_TRAINING_INFO,
    data: data
  };
};
export const clearTrainingInfo = () => {
  return {
    type: actionTypes.CLEAR_TRAINING_INFO,
  };
};



export const logout = () => {

  axiosInstance.post('api/user/logout/blacklist/',{
    refresh_token:localStorage.getItem('refresh_token'),
  });

  localStorage.removeItem("user");
  localStorage.removeItem("access_token");
  localStorage.removeItem("refresh_token");
  axiosInstance.defaults.headers['Authorization'] = null;
		// history.push('/admin/login');
    
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};
export const authLogin = (email, password, onSuccess = null, onError = null) => {
  console.log('saurav thakru')
  return dispatch => {
    dispatch(authStart());
    axiosInstance
      .post("api/token/", {
        email: email,
        password: password
      })
      .then(res => {
        console.log(res.data)
        console.log(res)
        const user = {
          token: res.data.access,
          email,
          userId: res.data.user,
        };
        localStorage.setItem("user", JSON.stringify(user));
        dispatch(authSuccess(user));
        axiosInstance.defaults.headers['Authorization'] = "Bearer " + res.data.access;


        console.log(res.data,'saurav thakur')
        localStorage.setItem("access_token", res.data.access);
        localStorage.setItem("refresh_token", res.data.refresh);
      console.log(localStorage.getItem('refresh_token'),'saurav thakur')



        if (typeof onSuccess == 'function') {
          onSuccess()
        }
      }
      )
      .catch(err => {
        console.log(err)
        dispatch(authFail(err));
        if (typeof onError == 'function') {
          onError()
        }

      });
  };
};

export const authSignUp = (email,username,password, onSuccess = null, onError = null) => {
  return dispatch => {
    dispatch(authStart());
    const user = { email,username,password };
    console.log(user)
    axiosInstance
			.post(`api/user/register/`, {
				email: email,
				user_name: username,
				password: password,
			})
      .then(res => {
        console.log(res)
        const user = {
          username,
          email,
          email,
          token: res.data.key,
          userId: res.data.user,
          expirationDate: new Date(new Date().getTime() + 3600 * 1000)
        };
        console.log(user.expirationDate)
        localStorage.setItem("user", JSON.stringify(user));
        dispatch(authSuccess(user));

        if (typeof onSuccess == 'function') {
          onSuccess()
        }
      }
      )
      .catch(err => {
        console.log(err)
        dispatch(authFail(err));
        if (typeof onError == 'function') {
          onError(err)
        }

      });
  };
};

export const authCheckState = () => {
  return dispatch => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user === undefined || user === null) {
      console.log('not found')
    } else {
      const expirationDate = new Date(user.expirationDate);

      console.log(new Date(user.expirationDate), new Date())
      // if (expirationDate <= new Date()) {
      //   dispatch(logout());
      // } else {
      //   dispatch(
      //     checkAuthTimeout(
      //       (expirationDate.getTime() - new Date().getTime()) / 1000
      //     )
      //   );
      // }
    }
  };
};