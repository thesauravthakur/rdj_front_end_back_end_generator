import axios from "axios";
import * as actionTypes from "./actionTypes";
import { useEffect } from "react";
import { excelFormula } from "react-syntax-highlighter/dist/cjs/languages/prism";
import { mainLink } from '../utils';
import { getSetting } from '../settings/action';






export const permissionGetStart = () => {
  return {
    type: actionTypes.PERMISSION_GET_START
  };
};

export const permissionGetSuccess = permissions => {
  return {
    type: actionTypes.PERMISSION_GET_SUCCESS,
    permissions
  };
};

export const permissionGetFail = error => {
  return {
    type: actionTypes.PERMISSION_GET_FAIL,
    error: error
  };
};













export const memberGetStart = () => {
  return {
    type: actionTypes.MEMBER_GET_START
  };
};

export const memberGetSuccess = members => {
  return {
    type: actionTypes.MEMBER_GET_SUCCESS,
    members
  };
};

export const memberGetFail = error => {
  return {
    type: actionTypes.MEMBER_GET_FAIL,
    error: error
  };
};






export const memberPostStart = () => {
  return {
    type: actionTypes.MEMBER_POST_START
  };
};

export const memberPostSuccess = member => {
  return {
    type: actionTypes.MEMBER_POST_SUCCESS,
    member
  };
};

export const memberPostFail = error => {
  return {
    type: actionTypes.MEMBER_POST_FAIL,
    error: error
  };
};








export const memberEditStart = () => {
  return {
    type: actionTypes.MEMBER_EDIT_START
  };
};

export const memberEditSuccess = member => {
  return {
    type: actionTypes.MEMBER_EDIT_SUCCESS,
    member
  };
};

export const memberEditFail = error => {
  return {
    type: actionTypes.MEMBER_EDIT_FAIL,
    error: error
  };
};












export const memberTypeGetStart = () => {
  return {
    type: actionTypes.MEMBERTYPE_GET_START
  };
};

export const memberTypeGetSuccess = membertypes => {
  return {
    type: actionTypes.MEMBERTYPE_GET_SUCCESS,
    membertypes
  };
};

export const memberTypeGetFail = error => {
  return {
    type: actionTypes.MEMBERTYPE_GET_FAIL,
    error: error
  };
};



export const memberTypePostStart = () => {
  return {
    type: actionTypes.MEMBERTYPE_POST_START
  };
};

export const memberTypePostSuccess = membertype => {
  return {
    type: actionTypes.MEMBERTYPE_POST_SUCCESS,
    membertype
  };
};

export const memberTypePostFail = error => {
  return {
    type: actionTypes.MEMBERTYPE_POST_FAIL,
    error: error
  };
};








export const memberTypeEditStart = () => {
  return {
    type: actionTypes.MEMBERTYPE_EDIT_START
  };
};

export const memberTypeEditSuccess = membertype => {
  return {
    type: actionTypes.MEMBERTYPE_EDIT_SUCCESS,
    membertype
  };
};

export const memberTypeEditFail = error => {
  return {
    type: actionTypes.MEMBERTYPE_EDIT_FAIL,
    error: error
  };
};












export const getpermission = (token) => {
  return dispatch => {
    dispatch(permissionGetStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/permission/")
      .then(res => {
        dispatch(permissionGetSuccess(res.data));
        console.log(res.data)

      }
      ).catch(err => {
        dispatch(permissionGetFail(err));


      });
  };
};
















export const getmember = (token) => {
  return dispatch => {
    dispatch(memberGetStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/member/")
      .then(res => {
        dispatch(memberGetSuccess(res.data));
        dispatch(getpermission(token))
        console.log(res.data)

      }
      ).catch(err => {
        dispatch(memberGetFail(err));


      });
  };
};





export const postmember = (member, token) => {

  console.log(member, token)
  return dispatch => {
    dispatch(memberPostStart());
    const data = {
      member_first_name: member.member_first_name,
      member_last_name: member.member_last_name,
      member_address: member.member_address,
      pnumber: member.pnumber,
      email: member.email,
      sanket_number: member.sanket_number,
      member_type: member.member_type,
      userId: member.userId,
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/member/", data)
      .then(res => {
        dispatch(getmember(token))
        dispatch(memberPostSuccess(data));



      }
      )
      .catch(err => {
        dispatch(memberPostFail(err));

      });
  };
};







export const editmember = (member, id, token, onSuccess = null, onError = null) => {
  return dispatch => {
    dispatch(memberEditStart());
    const data = {
      member_first_name: member.member_first_name,
      member_last_name: member.member_last_name,
      member_address: member.member_address,
      pnumber: member.pnumber,
      email: member.email,
      sanket_number: member.sanket_number,
      member_type: member.member_type,
      userId: member.userId,
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/member/${id}/`, data)
      .then(res => {
        console.log(res.data)
        dispatch(memberEditSuccess(res.data));
        dispatch(getmember(token))
        if (typeof onSuccess == 'function') {
          onSuccess()
        }
        else {
          onError()
        }


      }
      ).catch(err => {
        dispatch(memberEditFail(err));

      });
  };
};


export const deletemember = (memberId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(memberPostStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/member/${memberId}/`)
      .then(res => {
        dispatch(memberPostSuccess(memberId));
        dispatch(getmember(token))
        console.log(res)
      }
      )
      .catch(err => {
        dispatch(memberPostFail(err));
        console.log(err)
      });
  };
};


































export const getmemberType = (token) => {
  return dispatch => {
    dispatch(memberTypeGetStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/membertype/")
      .then(res => {
        dispatch(memberTypeGetSuccess(res.data));
        dispatch(getpermission(token))
        console.log(res.data)

      }
      ).catch(err => {
        dispatch(memberTypeGetFail(err));


      });
  };
};


export const postmemberType = (member_type, permissionsList, token, onSuccess = null, onError = null) => {

  console.log(member_type, token)
  return dispatch => {
    dispatch(memberTypePostStart());
    const data = {
      member_type: member_type.member_type,
      permissionsList: permissionsList
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/membertype/", data)
      .then(res => {
        dispatch(getmemberType(token))
        dispatch(memberTypePostSuccess(data));
        dispatch(getpermission(token))
        if (typeof onSuccess == 'function') {
          onSuccess()
        }
        else {
          onError()
        }




      }
      )
      .catch(err => {
        dispatch(memberTypePostFail(err));

      });
  };
};



export const getDetailmemberType = (token, memberTypeId) => {
  return (dispatch) => {
    dispatch(memberTypePostStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + `/api/v1/membertype/${memberTypeId}/`)
      .then((res) => {
        dispatch(memberTypePostSuccess(res.data));
        console.log(res.data)
      })
      .catch((err) => {
        dispatch(memberTypePostFail(err));
        console.log(err)
      });
  };
};



export const editmemberType = (id, state, permissions, token, onSuccess = null, onError = null) => {
  return dispatch => {
    dispatch(memberTypeEditStart());
    const data = {
      member_type: state.member_type,
      permissions: permissions,
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .put(mainLink + `/api/v1/membertype/${id}/`, data)
      .then(res => {
        console.log(res.data)
        dispatch(memberTypeEditSuccess(res.data));
        dispatch(getpermission(token))
        if (typeof onSuccess == 'function') {
          onSuccess()
        }
        else {
          onError()
        }


      }
      ).catch(err => {
        dispatch(memberTypeEditFail(err));

      });
  };
};


export const deletememberType = (memberId, token) => {
  console.log(token)
  return dispatch => {
    dispatch(memberTypePostStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .delete(mainLink + `/api/v1/membertype/${memberId}/`)
      .then(res => {
        dispatch(memberTypePostSuccess(memberId));
        dispatch(getmemberType(token))
        dispatch(getpermission(token))
        console.log(res)
      }
      )
      .catch(err => {
        dispatch(memberTypePostFail(err));
        console.log(err)
      });
  };
};





























