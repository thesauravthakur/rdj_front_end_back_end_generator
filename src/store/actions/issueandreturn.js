import axios from "axios";
import * as actionTypes from "./actionTypes";
import { getBookAuthor, getBookPublication, getBook, getBookType, getComponent, getBookEdition, getBookRack, getBookRoom, getBookSubCategory } from "./book";
import * as auth from '../reducers/auth';
import * as book from '../reducers/book';
import { mainLink } from '../utils';



export const getSettingStart = () => {
  return {
    type: actionTypes.GET_SETTING_START
  };
};

export const getSettingSuccess = setting => {
  return {
    type: actionTypes.GET_SETTING_SUCCESS,
    setting
  };
};

export const getSettingFail = error => {
  return {
    type: actionTypes.GET_SETTING_FAIL,
    error: error
  };
};






export const postSettingStart = () => {
  return {
    type: actionTypes.POST_SETTING_START
  };
};

export const postSettingSuccess = settingpost => {
  return {
    type: actionTypes.POST_SETTING_SUCCESS,
    settingpost
  };
};

export const postSettingFail = error => {
  return {
    type: actionTypes.POST_SETTING_FAIL,
    error: error
  };
};





export const getSetting = (token) => {
  return dispatch => {
    dispatch(getSettingStart());
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .get(mainLink + "/api/v1/settings/")
      .then(res => {
        dispatch(getSettingSuccess(res.data));
        console.log(res)

      }
      ).catch(err => {
        dispatch(getSettingFail(err));


      });
  };
};

export const postSetting = (state, token, onSuccess = null, onError = null) => {
  console.log(token)
  return dispatch => {
    dispatch(postSettingStart());
    const data = {
      print_on_submit: state.print_on_submit,
      library_name: state.library_name,
      library_address: state.library_address,
      library_phone_number: state.library_phone_number,
      print_size: state.print_size
    }
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${token}`,
    };
    axios
      .post(mainLink + "/api/v1/settings/", data)
      .then(res => {
        dispatch(postSettingSuccess(res.data));
        dispatch(getSetting(token));
        if (typeof onSuccess == 'function') {
          onSuccess()
        }

      }
      )
      .catch(err => {
        dispatch(postSettingFail(err));
        if (typeof onError == 'function') {
          onError()
        }

      });
  };
};