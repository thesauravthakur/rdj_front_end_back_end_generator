import React, { Fragment } from 'react';

import Chart from 'react-apexcharts';
import Nepali from 'nepalify-react';

export default function LivePreviewExample(props) {
  const options = {
    tooltip: {
      theme: 'dark',
      x: {
        show: false
      },
      y: {
        show: true
      }
    },
    colors: ["#519ca5", "#2274A5"],
    plotOptions: {
      bar: {
        horizontal: true,
        dataLabels: {
          position: "top"
        }
      }
    },
    chart: {
      toolbar: {
        show: true,
      }
    },
    dataLabels: {
      enabled: true,
      offsetX: 0,
      style: {
        fontSize: "12px",
        colors: ["#fff"]
      },
      // formatter: function (val, opt) {
      //   return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
      // },
    },
    stroke: {
      show: true,
      width: 1,
      colors: ["#fff"]
    },
    xaxis: {
      categories: [
        ["प्रदेश नं० १"],
        ["प्रदेश नं० २"],
        ["वाग्मती प्रदेश"],
        ["गण्डकी प्रदेश"],
        ["लुम्बिनी प्रदेश"],
        ["कर्णाली प्रदेश"],
        ["सुदूर-पश्चिम प्रदेश"]]
    },
    legend: {
      position: "right",
      markers: {
        width: 24,
        height: 24,
        strokeWidth: 0,
        strokeColor: "#fff",
        fillColors: undefined,
        radius: 2,
        customHTML: undefined,
        onClick: undefined,
        offsetX: 0,
        offsetY: 0,

      }
    }
  };
  // const preeti = require('preeti');
  const series = [
    {
      name: '',
      data: props.data
    }
  ];
  return (
    <Fragment >
      {/* {preeti('sf7df08"')} */}
      < Chart options={options} series={series} type="bar" />
    </Fragment >
  );
}
