import React from "react";
import background from "../../assets/VectorMap.png"
import logo from "../../assets/Image_1.png"
import { Link } from "react-router-dom";
import Toolbar from "@material-ui/core/Toolbar";
import { Grid, Typography } from "@material-ui/core";
import axios from "axios";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

export default class WelcomePage extends React.Component {
    state = {
        trainings: 0,
        showSnackBar: false,
        snackbarMessage: '',
        link: 'eKqWrEL-wgk'
    }

    componentDidMount() {
        document.getElementById('root').style.height = '100%';
        document.getElementById('root').querySelector('div').style.height = '100%';
        axios.defaults.headers = {
            "Content-Type": "application/json",
        };
        localStorage.setItem('trainingslen', 0)
        axios
            .get(window.location.origin + `/api/v1/trainee/trainings/?self&link`)
            .then(res => {
                if (typeof res.data === 'object') {
                    localStorage.setItem('trainingslen', res.data.total)
                    localStorage.setItem('link', res.data.link)
                    localStorage.setItem('parsedLink', this.parseVideoId(res.data.link))
                    this.setState({
                        trainings: res.data.total,
                        link:this.parseVideoId(res.data.link)
                    })
                }


            })
            .catch(err => {
                this.setState({
                    isErrorDialogPopUp: true,
                    ErrorMessage: 'Failed to load trainings'
                })
            })
    }

    parseVideoId(link){
        return link.split('?v=').pop().split('/')[0]
    }

    handleSnackClose = () => {
        this.setState({
            showSnackBar: false,
            snackbarMessage: ''

        })
    }

    render() {
        function MouseOver(event) {
            event.target.style.color = 'red';
        }
        function MouseOut(event) {
            event.target.style.color = '#3E3A67';
        }
        return (
            <div style={{ position: "fixed", top: 0, left: 0, minWidth: "100%", minHeight: '100%', backgroundRepeat: 'no-repeat', backgroundImage: 'url(' + background + ')', backgroundAttachment: 'fixed', backgroundPosition: 'center' }}>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={this.state.showSnackBar}
                    autoHideDuration={2000}
                    onClose={this.handleSnackClose}
                    message={this.state.snackbarMessage}
                    action={
                        <React.Fragment>
                            <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleSnackClose}>
                                <CloseIcon fontSize="small" />
                            </IconButton>
                        </React.Fragment>
                    }
                />

                <div style={{ margin: 'auto', position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, width: "800px", height: '650px', backgroundColor: 'rgba(222,233,247,0.898)' }}>
                    <div style={{ display: 'flex', justifyContent: 'center', float: 'left', marginTop: '30px', marginLeft: '24px' }}>
                        <img id="Image_1" style={{ width: '100px', height: '84px' }} src={logo} alt="Logo" />
                    </div>
                    <Toolbar style={{ float: 'right', marginTop: '30px' }}>
                        <Grid
                            justify="space-between" // Add it here :)
                            container
                            spacing={24}
                        >
                            <Grid item>
                            </Grid>

                            <Grid item>
                                <Typography variant="h6" color="inherit" noWrap>
                                    <img src="https://www.animatedimages.org/data/media/839/animated-nepal-flag-image-0007.gif" alt="logo" style={{ width: '60px' }} />
                                </Typography>
                            </Grid>
                        </Grid>
                    </Toolbar>

                    <div style={{
                        whiteSpace: "nowrap",
                        textAlign: 'center',
                        fontFamily: 'Helvetica Neue',
                        fontStyle: 'normal',
                        fontWeight: 'normal',
                        fontSize: '22px',
                        marginTop: '30px',
                        color: 'rgba(0,69,139,1)'
                    }}>
                        <span>नेपाल सरकार<br />भूमि व्यवस्था,सहकारी तथा गरिबी निवारण मन्त्रालय<br />सहकारी विभाग</span>
                    </div>
                    <div style={{
                        textAlign: 'center',
                        fontFamily: 'Helvetica Neue',
                        fontStyle: 'normal',
                        fontWeight: 'bold',
                        fontSize: '30px',
                        marginTop: '10px',
                        color: 'rgba(222,0,0,1)'
                    }}>
                        <span>सहकारी प्रशिक्षण तथा अनुसन्धान केन्द्र</span>
                    </div>
                    <div style={{ "overflow": "visible", "whiteSpace": "nowrap", "textAlign": "center", "fontFamily": "Arial", "fontStyle": "normal", "fontWeight": "bold", "fontSize": "30px", "color": "rgba(222,0,0,1)" }}>
                        <span>Cooperative Training and Research Centre</span>
                    </div>
                    <div style={{
                        whiteSpace: "nowrap",
                        textAlign: 'center',
                        fontFamily: 'Helvetica Neue',
                        fontStyle: 'normal',
                        fontWeight: 'normal',
                        fontSize: '22px',
                        marginTop: '4px',
                        color: 'rgba(0,69,139,1)'
                    }}>
                        <span>नयाँ वानेश्वर, काठमाडौं</span>
                    </div>
                    <br /><br /><br />
                    <Grid container>
                        <Grid item style={{ width: '40%', margin: 'auto' }}>
                            <div style={{ marginLeft: '50px' }}>
                                <div style={{
                                    fontFamily: 'Helvetica Neue',
                                    fontStyle: 'normal',
                                    fontWeight: '500',
                                    fontSize: '22px'
                                }}>
                                    Trainee Registration <br />(प्रशिक्षार्थि दर्ता गर्नुहोस्)
                                </div><br />
                                {
                                    this.state.trainings
                                        ? <Link to="/trainee/register" aria-controls="simple-menu" onMouseOver={MouseOver} onMouseOut={MouseOut} style={{ fontSize: '18px' }}>
                                            Register Now
                                            </Link>
                                        : <Link to="/trainee/register" aria-controls="simple-menu" onMouseOver={MouseOver} onMouseOut={MouseOut} style={{ fontSize: '18px' }} onClick={(event) => {
                                            event.preventDefault(); this.setState({
                                                showSnackBar: true,
                                                snackbarMessage: '\n' +
                                                    'माफ गर्नुहोस्, कुनै प्रशिक्षण भेटिएन !'
                                            })
                                        }}>
                                            Register Now
                                            </Link>

                                }
                                <br />
                                <br />
                                <br />
                                <br />
                                <Link to="/admin/login" aria-controls="simple-menu" style={{ fontSize: '18px', textAlign: 'center', color: '#3E3A67' }} onMouseOver={MouseOver} onMouseOut={MouseOut}>
                                    Admin Login
                                </Link><br /><br />
                            </div>
                        </Grid>
                        <Grid item style={{ width: '60%' }}>
                            <div style={{ marginRight: '50px' }}>
                                <iframe style={{ width: '100%', height: '240px' }} src={"https://www.youtube.com/embed/"+this.state.link+"?autoplay=1"} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen/>
                                <span>हाम्रो साइटमा प्रशिक्षणका लागि कसरी दर्ता गर्ने</span>
                            </div>
                        </Grid>
                    </Grid>

                </div>
            </div>
        );
    }
}