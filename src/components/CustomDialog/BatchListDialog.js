import React from 'react'
import Dialog from "@material-ui/core/Dialog";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {Box, Button} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import Slide from "@material-ui/core/Slide";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
class BatchListDialog extends React.Component{
    state = {
        batch: ''
    }
    render() {
        const {open,batches,handleClose,handleCloseSuccess,handleBatchChange} = this.props

        return (<Dialog
            open={open}
            onClose={handleClose}
            TransitionComponent={Transition}
            // PaperComponent={PaperComponent}
            aria-labelledby="draggable-dialog-title"
        >
            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                Select Batches
            </DialogTitle>
            <DialogContent>
                <Box width="100%">
                    <Autocomplete
                        id="combo-box-demo"
                        options={batches}
                        inputValue={this.state.batch}
                        onInputChange={(event, newInputValue) => {handleBatchChange(event, newInputValue.toUpperCase());this.setState({
                            batch:newInputValue
                        })}}
                        style={{ width: 300 }}
                        renderInput={(params) => <TextField {...params} label="Batches" variant="outlined" />}
                    />
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseSuccess} color="primary" autoFocus>
                    Ok
                </Button>
            </DialogActions>
        </Dialog>)
    }
}
export default BatchListDialog