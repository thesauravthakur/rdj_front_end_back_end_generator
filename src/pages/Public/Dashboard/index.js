import React from 'react';
import { Fragment } from 'react';
import Footer from '../Footer/index';
import SirImg from '../../../assets/images/PkkImage/testimg.jpg';
import hero12 from '../../../assets/images/hero-bg/hero-12.png';
import hero9 from '../../../assets/images/hero-bg/hero-9.jpg';
import hero8 from '../../../assets/images/hero-bg/hero-8.jpg';
import hero10 from '../../../assets/images/hero-bg/hero-10.jpeg';
import bg1 from '../../../assets/images/composed-bg/bg1.jpg';
import bg2 from '../../../assets/images/composed-bg/bg2.jpg';
import bg5 from '../../../assets/images/composed-bg/bg5.jpg';
import bg3 from '../../../assets/images/composed-bg/bg3.jpg';
import bg4 from '../../../assets/images/composed-bg/bg4.jpg';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Row, Col, Divider } from 'antd';
import "antd/dist/antd.css";
import Tooltip from '@material-ui/core/Tooltip';
import { Carousel as SingleCarousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import axios from 'axios';
class PublicDashboard extends React.Component {
  state = {
    playlistsIds: [],
    videos: [],
    latestVideos: [],
    sliderData: [],
    current_playlist: '',
    current_video: null,
    channelId: 'UCz_jZRh5ybx8Wa-Xlrzba5w',
    search: null,
    key: 'AIzaSyBqdgAaUlrWDo1I5IsbW7lTJadvdHQmWrw'
  }
  // AIzaSyBmflSvCF0p9wcVS6JSYB3KnaNNrkijZQA
  async componentDidMount() {
    console.log('componentDidMount colling ...');

    // axios.defaults.headers = {
    //   "Content-Type": "application/json",
    //   Authorization: `Token ${token}`,
    // };


    axios
      .get(window.location.origin + "/api/v1/PublicSlider/")
      .then(res => {
        this.setState({ loading: false })

        let data = res.data
        console.log(data)
        this.setState({
          sliderData: res.data
        })
      }
      )
      .catch(err => {
        this.setState({ loading: false })

        // console.log(err)
        // this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

      });
    axios
      .get(window.location.origin + "/api/v1/settingsData/")
      .then(res => {
        console.log(res.data)
        let key = 'test'
        let channellink = ''
        if (res.data.google_key) {
          this.setState({ key: res.data.google_key })
        }
        if (res.data.video_link) {
          channellink = res.data.video_link
          let channelId = channellink.split('/')[channellink.split('/').length - 1]
          this.setState({ channelId: channelId })
        }

      }
      ).catch(err => {
        console.log(err)
      })
    let playlistParams = {
      part: 'contentDetails,snippet,localizations,player',
      channelId: this.state.channelId,
      key: this.state.key,
      maxResults: 100,
    }
    axios.get('https://youtube.googleapis.com/youtube/v3/playlists', {
      params: playlistParams
    })
      .then(res => {
        for (let i = 0; i < res.data.items.length; i++) {
          // console.log(res.data.items[i].snippet.title)
          let obj = {}
          obj['playListId'] = res.data.items[i].id
          obj['playListTitle'] = res.data.items[i].snippet.title
          this.state.playlistsIds.push(obj)
          this.setState({ playlistsIds: this.state.playlistsIds })
          this.getVideosForPlaylist(100, res.data.items[i].id, this.state.key, res.data.items[i].snippet.title)
        }
        // console.log(this.state.playlistsIds)        
      }).catch(e => {
        console.log(e)
      })



    axios.get('https://youtube.googleapis.com/youtube/v3/search', {
      params: {
        part: 'snippet',
        channelId: this.state.channelId,
        key: this.state.key,
        maxResults: 1000,
        order: 'date'
      }
    })
      .then(res => {
        // console.log(res.data.items)  
        this.setState({ latestVideos: res.data.items })
      }).catch(e => {
        console.log(e)
      })

  }
  // Tempkey='AIzaSyBqdgAaUlrWDo1I5IsbW7lTJadvdHQmWrw'
  getVideosForPlaylist = (limit = 100, playlistId = 'UUz_jZRh5ybx8Wa-Xlrzba5w', key = this.state.key, title = 'test') => {
    let playlistParams = {
      part: 'snippet',
      playlistId: playlistId,
      maxResults: limit,
      key: key
    }
    axios.get('https://www.googleapis.com/youtube/v3/playlistItems', {
      params: playlistParams
    })
      .then(res => {
        if ('items' in res.data) {
          this.state.videos.push(res.data.items)
          this.setState({ videos: this.state.videos })
          // console.log(this.state.videos)
        }
      })

  }







  render() {
    const responsive = {
      type0: {
        breakpoint: { max: 3000, min: 1305 },
        items: 5,
        slidesToSlide: 1 // optional, default to 1.
      },
      type1: {
        breakpoint: { max: 1305, min: 1062 },
        items: 4,
        slidesToSlide: 1 // optional, default to 1.
      },
      type2: {
        breakpoint: { max: 1062, min: 800 },
        items: 3,
        slidesToSlide: 1 // optional, default to 1.
      },
      type3: {
        breakpoint: { max: 800, min: 600 },
        items: 2,
        slidesToSlide: 1 // optional, default to 1.
      },
      type4: {
        breakpoint: { max: 600, min: 0 },
        items: 1,
        slidesToSlide: 1 // optional, default to 1.
      }
    };
    return (
      <Fragment>

        <div style={{height:'500px',color:'white',textAlign:'center'}}>
        Saurav' Dashboard
        </div>

      </Fragment>
    )
  }
}
export default PublicDashboard;
