import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody, MDBIcon, MDBCardHeader } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import { Select } from 'antd';
import "antd/dist/antd.css";
import Tooltip from '@material-ui/core/Tooltip';
import { Hidden } from '@material-ui/core';
import ReCAPTCHA from "react-recaptcha"
import axios from 'axios';
import { tuple } from 'antd/lib/_util/type';
const { Option } = Select;
class PublicContact extends React.Component {
    state = {
        subject: '',
        name: '',
        email: '',
        message: '',
        emailVerification: false,
        data: {},
        nameRequired: false,
        messageRequired: false,
        emailRequired: false,
        subjectRequired: false,
        recaptchaVerification: '',
        recaptchaRequired: false,

    }
    async componentDidMount() {
        console.log('componentDidMount colling ...');

        // axios.defaults.headers = {
        //   "Content-Type": "application/json",
        //   Authorization: `Token ${token}`,
        // };
        axios
            .get(window.location.origin + "/api/v1/settingsData/")
            .then(res => {
                // console.log(res.data)
                this.setState({ data: res.data })

            }
            ).catch(err => {
                console.log(err)
            })
    }
    render() {
        const handelemailchange = (e) => {
            this.setState({ email: e.target.value })
            if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.target.value)) {
                this.setState({ emailVerification: true })
            }
            else {
                this.setState({ emailVerification: false })
                console.log('Correct Email')
            }
        }
        const Submit = (value) => {
            console.log("Captcha value:", this.state)
            if (this.state.subject == '') {
                this.setState({ subjectRequired: true })
            }
            else if (this.state.name == '') {
                this.setState({ nameRequired: true })
            }
            else if (this.state.email == '') {
                this.setState({ emailRequired: true })
            }
            else if (this.state.message == '') {
                this.setState({ messageRequired: true })
            }
            else if (this.state.emailVerification == true) {
                this.setState({ emailRequired: true })
            } else if (this.state.recaptchaVerification == '') {
                this.setState({ recaptchaRequired: true })
            }
            else {
                let data = {
                    'subject': this.state.subject,
                    'email': this.state.email,
                    'name': this.state.name,
                    'message': this.state.message,
                }
                axios
                    .post(window.location.origin + "/api/v1/PublicContactRequest/", data)
                    .then(res => {
                        this.setState({
                            name: '',
                            email: '',
                            message: '',
                            // subject: 'CHOOSE A SUBJECT',
                            subjectRequired: false,
                            nameRequired: false,
                            emailRequired: false,
                            messageRequired: false
                        })
                        resetCaptcha();

                    }
                    ).catch(err => {
                        console.log(err)
                    })
            }
        }
        // specifying your onload callback function
        var callback = function () {
            console.log('Done!!!!');
        };
        let captcha;
        const setCaptchaRef = (ref) => {
            if (ref) {
                return captcha = ref;
            }
        };
        const resetCaptcha = () => {
            // maybe set it till after is submitted
            captcha.reset();
        }
        // specifying verify callback function
        const verifyCallback = (response) => {
            this.setState({ recaptchaVerification: response })
            this.setState({ recaptchaRequired: false })
            console.log(response);
            console.log(this.state);
        };
        return (
            <div className='public-contact' style={{ marginTop: '10px' }}>
                <div className='public-contact-container' >
                    <MDBRow >
                        <MDBCol md="5" lg='4' >
                            <Hidden mdDown>
                                <div className='public-contact-container-text-div'>
                                    <h2>Contact  <span style={{ fontWeight: 'bold' }}>P</span><span style={{ color: '#ffc105', fontWeight: 'bold' }}>K</span><span style={{ fontWeight: 'bold', color: '#ffc105' }}>K</span></h2>
                                    <h4>PKK is a premium content destination from The Prabidhi Kaa Kura.</h4>
                                    <address>
                                        Prabidhi Kaa Kura <br />
                                        {this.state.data.address ? this.state.data.address : 'Kathamandu Nepal'}<br />
                            Phone: {this.state.data.phone_number ? this.state.data.phone_number : '+977-9808853237'}
                                    </address>
                                </div>
                            </Hidden>

                        </MDBCol>
                        <MDBCol md="12" lg='4' className='col-test' >
                            <MDBCard >
                                <MDBCardBody className='public-contact-card-body' >
                                    <Select
                                        style={{ paddingBottom: 0, marginBottom: 0 }}

                                        onChange={(data) => this.setState({ subject: data })} id='subject' defaultValue='CHOOSE A SUBJECT' style={{ backgroundColor: 'black', width: '100%' }}>
                                        <Option value="Business">BUSINESS</Option>
                                        <Option value="Marketing">MARKETING</Option>
                                        <Option value="Pr">PR</Option>
                                        <Option value="Technical">TECHNICAL</Option>
                                    </Select>
                                    {!this.state.subject &&
                                        this.state.subjectRequired &&
                                        <span id="emailHelp" style={{ paddingTop: 0, marginTop: 0, color: 'rgb(255, 193, 5) ' }} className="form-text">
                                            Subject Required*
                                     </span>
                                    }
                                    <MDBInput
                                        label="TYPE YOUR NAME"
                                        group
                                        onChange={(e) => this.setState({ name: e.target.value })}
                                        type="text"
                                        validate
                                        valueDefault={this.state.name}
                                        value={this.state.name}
                                        error="wrong"
                                        success="right"
                                        className='public-contact-card-body-MDBInput'
                                        style={{ paddingBottom: 0, marginBottom: 0 }}


                                    />
                                    {!this.state.name &&
                                        this.state.nameRequired &&
                                        <span id="emailHelp" style={{ color: 'rgb(255, 193, 5) ' }} className="form-text">
                                            Name Required*
                                     </span>
                                    }
                                    <MDBInput
                                        label="TYPE YOUR EMAIL HERE"
                                        group
                                        type="email"
                                        onChange={handelemailchange}
                                        validate
                                        valueDefault={this.state.email}
                                        value={this.state.email}
                                        error="wrong"
                                        success="right"
                                        className='public-contact-card-body-MDBInput'
                                        style={{ paddingBottom: 0, marginBottom: 0 }}

                                    />
                                    {
                                        this.state.emailVerification &&
                                        <span id="emailHelp" style={{ paddingTop: 0, marginTop: 0, color: 'rgb(255, 193, 5) ' }} className="form-text">
                                            Invalid Email Address
                                     </span>
                                    }
                                    {!this.state.email &&
                                        this.state.emailRequired &&
                                        <span id="emailHelp" style={{ paddingTop: 0, marginTop: 0, color: 'rgb(255, 193, 5) ' }} className="form-text ">
                                            Valid Email Required*
                                     </span>
                                    }
                                    <MDBInput
                                        label="TYPE YOUR MESSAGE HERE..."
                                        group
                                        type="textarea"
                                        validate
                                        valueDefault={this.state.message}
                                        value={this.state.message}
                                        onChange={(e) => this.setState({ message: e.target.value })}
                                        error="wrong"
                                        success="right"
                                        rows="5"
                                        className='public-contact-card-body-MDBInput-textarea'
                                        style={{ paddingBottom: 0, marginBottom: 0 }}

                                    />
                                    {!this.state.message &&
                                        this.state.messageRequired &&
                                        <span id="emailHelp" style={{ paddingTop: 0, marginTop: 0, color: 'rgb(255, 193, 5) ' }} className="form-text ">
                                            Message Required*
                                     </span>
                                    }
                                    <ReCAPTCHA
                                        ref={(r) => setCaptchaRef(r)}
                                        style={{ marginTop: 10 }}
                                        sitekey="6LfqlFgaAAAAAPugjSE_0YLWWL0JvdGOdBl-W8oS"
                                        render="explicit"
                                        verifyCallback={verifyCallback}
                                        onloadCallback={callback}
                                    />
                                    {this.state.recaptchaRequired &&
                                        <span id="emailHelp" style={{ paddingTop: 0, marginTop: 0, color: 'rgb(255, 193, 5) ' }} className="form-text ">
                                            Please fill up the Captcha*
                                     </span>
                                    }
                                    <div className="text-center mt-4">
                                        <div class="share-button" style={{ marginBottom: 0, paddingBottom: 0, float: 'right', marginTop: 0 }}>
                                            <span>SEND</span>
                                            <Tooltip title="SEND" arrow>

                                                <a style={{ color: 'black' }} type='submit' onClick={Submit} ><i class="fas fa-paper-plane"></i></a>
                                            </Tooltip>
                                        </div>
                                    </div>

                                </MDBCardBody>
                            </MDBCard>
                        </MDBCol>
                    </MDBRow>
                </div>
            </div>

        )
    }
}

export default PublicContact;
