import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody,MDBIcon,MDBCardHeader } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
import { Select } from 'antd';
import "antd/dist/antd.css";
import Tooltip from '@material-ui/core/Tooltip';
import { Hidden } from '@material-ui/core';
import ReCAPTCHA from "react-google-recaptcha"
import axios from 'axios';
const { Option } = Select;
class PublicContact extends React.Component {
    state={
        data:{},
    }
    async componentDidMount() {
        console.log('componentDidMount colling ...');
    
        // axios.defaults.headers = {
        //   "Content-Type": "application/json",
        //   Authorization: `Token ${token}`,
        // };
        axios
          .get(window.location.origin + "/api/v1/settingsData/")
          .then(res => {
            console.log(res.data)
            this.setState({data:res.data})
    
          }
          ).catch(err => {
            console.log(err)
          })
        }
    render() {
        function onChange(value) {
            console.log("Captcha value:", value)
          }
        return (
            <div className='public-about-us'style={{marginTop:'10px'}}>
            <div className='public-about-us-container' >
               <div className='public-about-us-container-header'>
               <h2>About P<span style={{color:'#ffc105'}}>K</span><span style={{color:'#ffc105'}}>K</span></h2>
               </div>
               <div className='public-about-us-container-content'>
               <p>PKK is a premium content destination from Prabidhi Kaa Kura.</p>
               <p>It caters to all those who want to have a premium content experience, but cannot find anything worth watching in the traditional channels.</p>
               </div>
            </div>
            </div>
     
        )
    }
}

export default PublicContact;
