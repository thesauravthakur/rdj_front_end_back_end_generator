import React from 'react';
import { Fragment } from 'react';
import { Link } from 'react-router-dom';

class PublicFooter extends React.Component {
    render() {
        return (
           <Fragment>

                    <div id="public-footer" style={{paddingTop:'50px'}}>
                    
                    <div  style={{textAlign:'center'}}>
                    <Link to='/public'>
                            <span className='public-footer-span' style={{}}>HOME</span>
                    </Link>
                    <Link to='/public/about-us'>
                            <span  className='public-footer-span' style={{paddingLeft:'50px'}} >ABOUT US</span>
                    </Link>
                            <Link to='/public/contact'>

                            
                            <span className='public-footer-span' style={{paddingLeft:'50px'}}>CONTACT US</span>
                            </Link>
                            
                    
                    </div>
                    <div style={{textAlign:'center',paddingTop:'50px'}}>
                        {/* <span>© 2020 | ALL RIGHT RESERVED TO STS</span> */}
                    </div>
                </div>
               
           </Fragment>
        )
    }
}

export default PublicFooter;