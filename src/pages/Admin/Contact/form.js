import React, { Fragment } from 'react';
import { WrapperSimple } from '../../../layout/Admin';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { PageTitle } from '../../../layout/Admin';
import { Router, browserHistory, IndexRoute } from 'react-router'
import { initialState } from '../../../store/reducers/book'
import { authCheckState } from '../../../store/actions/auth';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { mainLink } from '../../../store/utils'
import Alert from '@material-ui/lab/Alert';
import Hoc from '../../../hoc/hoc'
import {
  Checkbox,
  Icon,
  Divider,
  Paper,
  FormControlLabel
} from '@material-ui/core';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import hero9 from '../../../assets/images/hero-bg/hero-8.jpg';
import { SignUpForm } from '../../../components/FormFields/index';
import { postBookEdition, getBookEdition, deleteBookEdition, editBookEdition, getDetailBookEdition } from '../../../store/actions/book'
import { compose } from 'redux';
import axios from 'axios'
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Input } from 'antd';
import 'antd/dist/antd.css';
import { Upload, Button } from 'antd';
import MultiImageInput from 'react-multiple-image-input';
const { TextArea } = Input;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100ch'
    },
  },
});



const detaildetition = {}
class CategoryForm extends React.Component {
  constructor(props) {
    super(props);
    let id = this.props.match.params.id
    console.log(id)

  }
  state = {
    created_by: '',
    description: '',
    images: {},
  }


  componentDidMount() {
    this.props.authCheckState()
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    if (this.props.match.params.id != null && this.props.match.params.id != undefined) {
      this.setState({ loading: true })
      axios
        .get(window.location.origin + `/api/v1/PublicSlider/${this.props.match.params.id}/`)
        .then(res => {
          this.setState({ loading: false })
          let data = res.data
          console.log(data)
          this.setState({
            description: data.image_discription,
            created_by: data.image_created_by,
            images: { 0: data.image }
          })
        }
        )
        .catch(err => {
          this.setState({ loading: false })

          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })


        });
    }
  }



  render() {

    const handelSubmit = (e) => {
      e.preventDefault();
      let data = new FormData();
      let test = new FormData();
      // data.append('userId', this.props.userId)
      // data.append('description', this.state.description)
      // data.append('images', this.state.images)
      // console.log(this.state.imaimagesg)
      let temp = {
        'userId': this.props.userId,
        'description': this.state.description,
        'image': this.state.images,
        'created_by': this.state.created_by,

      }
      console.log(temp)

      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      axios
        .post(mainLink + "/api/v1/Slider/", temp)
        .then(res => {
          data = res.data
          console.log(data)
          if (data.required == 'image') {
            this.setState({ isErrorDialogPopUp: true, ErrorMessage: 'Image is Required' })
          }
          else if (data.required != 'image' && data.required) {
            document.getElementById(data.required).focus();
          }
          else if (data.success) {
            this.setState({ redirect: '/admin/Slider' });
          }
        }
        )
        .catch(err => {
          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })
        });


      // this.setState({ redirect: '/admin/traning' });


    }
    const handelSubmitAgain = (e) => {
      console.log(this.state)
      e.preventDefault();
      let data = new FormData();
      let temp = {
        'userId': this.props.userId,
        'description': this.state.description,
        'image': this.state.images,
        'created_by': this.state.created_by,

      }
      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      axios
        .post(mainLink + "/api/v1/Slider/", temp)
        .then(res => {
          data = res.data
          console.log(data)
          if (data.required == 'image') {
            this.setState({ isErrorDialogPopUp: true, ErrorMessage: 'Image is Required' })
          }
          else if (data.required != 'image' && data.required) {
            document.getElementById(data.required).focus();
          }
          else if (data.success) {
            this.setState({
              created_by: '',
              description: '',
              images: {},
            })

          }
        }
        )
        .catch(err => {
          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

        });

    }
    const handelUpdate = () => {

      let data = new FormData();
      // data.append('name', this.state.name);
      // data.append('userId', this.props.userId)
      // data.append('description', this.state.description)
      // data.append('images', this.state.images)
      let temp = {
        'userId': this.props.userId,
        'description': this.state.description,
        'image': this.state.images,
        'created_by': this.state.created_by,

      }
      console.log(this.state.images)
      if (this.state.emailVerification) {
        this.setState({ isErrorDialogPopUp: true, ErrorMessage: 'कृपया मान्य ईमेल ठेगाना दिनुहोस्' })

      } else {
        axios
          .put(window.location.origin + `/api/v1/Slider/${this.props.match.params.id}/`, temp)
          .then(res => {
            console.log(res.data)
            let data = res.data
            if (data.required == 'image') {
              this.setState({ isErrorDialogPopUp: true, ErrorMessage: 'Image is Required' })
            }
            else if (data.required != 'image' && data.required) {
              document.getElementById(data.required).focus();
            }
            else if (data.success) {
              this.setState({ redirect: `/admin/Slider/` });
            }
          }).catch(err => {
            console.log(err)
          })
      }

    }

    const { classes } = this.props;
    const { error } = this.props;
    var { teptitle } = this.state
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };
    const crop = {
      unit: '%',
      aspect: 16 / 9,
      width: '100'
    };
    return (
      <Hoc>
        <Dialog
          open={this.state.isErrorDialogPopUp}
          onClose={handleErrorClose}
          TransitionComponent={Transition}
          // PaperComponent={PaperComponent}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            Error Message
              </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleErrorClose} color="primary" autoFocus>
              Ok
                </Button>
          </DialogActions>
        </Dialog>
        <Card variant="outlined" >
          <div style={{ float: 'right', padding: 15 }}>
            <Link to="/admin/Slider" ><ArrowBackIcon style={{}} /> </Link>
          </div>
          <div className="font-size-lg font-weight-bold" style={{ padding: 10 }}>
            {this.props.match.params.id != null && this.props.match.params.id != undefined ? "EDIT SLIDER" : 'ADD SLIDER'}

          </div>

          <Divider className="my-1" />
          <CardContent style={{ padding: 5 }}>
            <CardContent className={classes.root} noValidate autoComplete="off">
              <MultiImageInput
                max={100}
                autoFocus

                images={this.state.images}
                disabled={true}
                setImages={(setImages) => {
                  console.log(setImages);
                  this.setState({ images: setImages })
                }}
                // theme="light" 
                theme={{
                  background: '#ffffff',
                  outlineColor: '#111111',
                  textColor: 'rgba(255,255,255,0.6)',
                  buttonColor: '#ff0e1f',
                  modalColor: '#ffffff'
                }}

                cropConfig={{ crop }}
              />
              <FormControlLabel
                control={
                  <Hoc>

                    <TextArea id="description"
                      defaultValue={this.state.name}
                      placeholder="DESCRIPTION" type='text' name='DESCRIPTION'
                      value={this.state.description}
                      style={{ width: "50%" }} onChange={(e) => { this.setState({ description: e.target.value }) }} />

                  </Hoc>
                }
              />
              <FormControlLabel
                control={
                  <Hoc>
                    <Input id="created_by"
                      defaultValue={this.state.created_by}
                      placeholder="CREATED BY" type='text' name='created_by'
                      value={this.state.created_by}
                      style={{ width: "50%" }} onChange={(e) => { this.setState({ created_by: e.target.value }) }} />
                  </Hoc>
                }
              />

              {this.props.match.params.id != null && this.props.match.params.id != undefined ?
                <div style={{ width: '500px' }}>

                  <Button
                    type='submit'
                    variant="outlined"
                    onClick={handelUpdate}
                  >
                    SUBMIT</Button>
                </div>
                :
                <div style={{ width: '500px' }}>
                  <Hoc>
                    <Button
                      type='submit'
                      variant="outlined"
                      onClick={handelSubmit}
                    >
                      SUBMIT</Button>
                    <Button variant="outlined"
                      onClick={handelSubmitAgain}
                      style={{ marginLeft: 10 }}>
                      SUBMIT AND ADD AGAIN</Button>
                  </Hoc>
                </div>

              }
            </CardContent>
          </CardContent>
        </Card>
      </Hoc >
    )
  }
}
const mapStateToProps = state => {
  return {
    token: state.auth.token,
    userId: state.auth.userId,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    authCheckState: () => dispatch(authCheckState())
  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(CategoryForm);