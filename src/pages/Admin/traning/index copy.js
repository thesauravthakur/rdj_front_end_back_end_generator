import React, { Fragment } from 'react';
import MaterialTable from 'material-table'; import { connect } from "react-redux";
import {
  Button
} from '@material-ui/core';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';
import Hoc from '../../../../hoc/hoc'
import { MTableToolbar } from 'material-table';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import { getBookSubCategory, postBook, getBookAuthor, editBook, getBook, deleteBook, getBookEdition, getBookEntryBatches, printBatchCodes } from '../../../../store/actions/book'
import { authCheckState } from '../../../../store/actions/auth';
import { compose } from 'redux';
import { BatchDialog, BatchListDialog } from "../../../../components/CustomDialog";

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
function PaperComponent(props) {
  return (

    <Paper {...props} />

  );
}
export const validate = (values, requiredfields) => {
  const errors = {}
  const requiredFields = requiredfields
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address'
  }
  return errors
}

var run = true






class BookCategory extends React.Component {
  // constructor(props) {
  //   super();
  //   console.log(window.location.pathname)
  //   if (window.location.pathname == '/baseUrl/admin/book') {
  //     window.addEventListener('keydown', (event) => {
  //       if (event.keyCode == '78') {
  //         return this.props.history.push("/admin/book-entry/")
  //       }
  //     });
  //   }
  // }
  state = {
    data: [],
    batch: null,
    count: 0,
    subcategorys: [],
    editions: [],
    open: false,
    hasErrorMessage: false,
    isErrorDialogPopUp: false,
    isPrintDialogOpen: false,
    isBatchListDialogOpen: false,
    batches: [],
    ErrorMessage: '',
    selectedRow: '',
    totalBooks: 0


  }
  componentDidMount() {
    var nextProps = this.props
    this.setState({
      data: nextProps.books, subcategorys: nextProps.subcategorys, editions: nextProps.editions

    })
    this.props.authCheckState()
  }
  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    console.log("willmount")
    if (!this.state.hasErrorMessage && nextProps.error === null) {
      this.state.hasErrorMessage = false
      this.setState({ open: true });
    } else {
      if (nextProps.error != null && this.props.error != nextProps.error) {
        console.log(nextProps.error)
        this.state.hasErrorMessage = true
        // if ('message' in nextProps.error && nextProps.error.message.split(' ').includes('500')) {
        console.log(nextProps.error.message)
        this.state.isErrorDialogPopUp = true
        this.state.ErrorMessage = "Sorry You Can't Delete This Book, Book Is Related"

        // }
        // else {
        //   this.state.ErrorMessage = nextProps.error.message
        // }
      }
    }
    var prevState = this.state
    if (
      nextProps.books !== prevState.data
      || nextProps.subcategorys != prevState.subcategorys ||
      prevState.editions != nextProps.editions) {
      this.setState({
        data: nextProps.books, subcategorys: nextProps.subcategorys, editions: nextProps.editions

      })
    }
  }


  render() {

    const currentBookPage = window.localStorage.getItem('current_book_page') ? parseInt(window.localStorage.getItem('current_book_page')) : 0
    console.log('currrent book page', currentBookPage)

    const { subcategorys } = this.state
    const { editions } = this.state
    var subcategoryslookupfields = []
    var editionlookupfields = []

    if (subcategorys.length > 0) {
      subcategorys.map((item, index) => (
        subcategoryslookupfields[item.id] = (item.name)
      ))
    }
    if (editions.length > 0) {
      editions.map((item, index) => (
        editionlookupfields[item.id] = (item.title)
      ))
    }

    const variants = ['h1', 'h3', 'body1', 'caption'];
    const da = true
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };
    return (
      <Hoc>
        <Fragment>
          <BatchDialog open={this.state.isPrintDialogOpen}
            classes={this.props.classes}
            printHtml={this.props.printHtml}
            handleClose={() => {
              this.setState({
                isPrintDialogOpen: false,
              })
            }} />
          <BatchListDialog
            open={this.state.isBatchListDialogOpen}
            batches={this.state.batches}
            handleClose={() => {
              this.setState({
                isBatchListDialogOpen: false
              })
            }}
            handleCloseSuccess={() => {
              this.setState({
                isBatchListDialogOpen: false
              })
              if (typeof this.state['isEditBatch'] !== 'undefined' && this.state.isEditBatch) {
                this.setState({
                  isEditBatch: false
                })
                if (this.state.batch != null && this.state.batch !== '') {
                  window.localStorage.setItem('batchId', this.state.batch)
                  this.props.history.push(`/admin/book-entries-edit/${this.state.editBatchId}`)
                }
              } else {

                if (this.state.batch != null && this.state.batch !== '') {
                  this.props.printBatchCodes([this.state.batch], this.props.token,
                    () => {
                      this.setState({
                        isPrintDialogOpen: true,
                      })
                    }, () => {
                      this.setState({
                        isErrorDialogPopUp: true,
                        ErrorMessage: "Sorry, Something went wrong!"
                      })
                    })
                }
              }
            }}
            handleBatchChange={(event, value) => {
              console.log(value)

              this.setState({
                batch: value
              })

            }}
          />
          <Dialog
            open={this.state.isErrorDialogPopUp}
            onClose={handleErrorClose}
            TransitionComponent={Transition}
            // PaperComponent={Paper  Component}
            aria-labelledby="draggable-dialog-title"
          >
            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
              Error Message
              </DialogTitle>
            <DialogContent>
              <DialogContentText>
                <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleErrorClose} color="primary" autoFocus>
                Ok
                </Button>
            </DialogActions>
          </Dialog>
          {/*<DataTable></DataTable>*/}
          <MaterialTable
            tableRef={this.tableRef}
            title={<a style={{ fontSize: 20, fontWeight: 'bold' }}>BOOK <span style={{ fontSize: 15, fontWeight: '100' }}>(Total: {this.state.totalBooks} books)</span></a>}
            columns={[
              {
                title: 'S.N',
                // field: 'title',
                // emptyValue: "",
                render: rowData => <span>{this.state.page + this.state.data.indexOf(rowData) + 1}</span>,
                // cellStyle: { , width: 10 },
                cellStyle: { textAlign: 'center' },
                headerStyle: { width: '10px', padding: '0px', textAlign: 'center' },

              },
              {
                title: `Title`,
                field: 'title',
                emptyValue: "",
                // render: rowData => <span>{rowData.title.toUpperCase()}</span>

                headerStyle: { width: '300px', textAlign: 'center' },

              },
              {
                title: 'Author',
                field: 'author',
                emptyValue: "",
                headerStyle: { width: '190px' }


              },
              {
                title: 'Language',
                field: 'language',
                emptyValue: "",
                headerStyle: { width: '50px' },
                cellStyle: { textAlign: 'center' },

                render: rowData => <span>{rowData.language.toUpperCase()}</span>

              },
              // {
              //   title: 'ISBN Number',
              //   field: 'isbnNumber',
              //   emptyValue: "",
              //
              // },
              {
                title: 'Publication',
                field: 'publication_name',
                emptyValue: "",
                headerStyle: { width: "190px" },
                render: rowData => <span>{rowData.publication_name.toUpperCase()}</span>

              },
              // {
              //   title: 'Book Type',
              //   field: 'booktype_name',
              //   emptyValue: "",
              //   render: rowData => <span>{rowData.booktype_name.toUpperCase()}</span>
              //
              // },
              // {
              //   title: 'Published Date',
              //   field: 'publishedDate',
              //   emptyValue: "",
              //   type: 'date'
              // },
              {
                title: 'Edition',
                field: 'edition_name',
                emptyValue: "",
                headerStyle: {},
                render: rowData => <span>{rowData.edition_name.toUpperCase()}</span>


              },
              {
                title: 'Number Of Page',
                field: 'numberOfPage',
                headerStyle: { width: '10px', textAlign: 'center' },
                cellStyle: { textAlign: 'center' },
                emptyValue: "",

              },
              {
                title: 'Sub Category',
                field: 'subcategory_name',
                emptyValue: "",
                headerStyle: {},
                render: rowData => <span>{rowData.subcategory_name.toUpperCase()}</span>


              },
              {
                title: 'Total Books',
                field: 'number_of_books',
                headerStyle: { width: '10px' },
                cellStyle: { textAlign: 'center' },
                emptyValue: "",

              },

            ]}
            onSearchChange={(res) => {
              console.log(res)
            }}
            data={
              query =>

                new Promise((resolve, reject) => {
                  let url = '/api/v1/book/?'
                  url += 'page_size=' + query.pageSize
                  url += '&page=' + (query.page + 1)
                  url += '&ordering=' + '-createdAt'
                  if (query.search.length) {
                    url += '&search=' + query.search
                  }
                  console.log('print', query)
                  fetch(url)
                    .then(response => response.json())
                    .then(result => {
                      this.setState({ data: result.results, page: query.page * query.pageSize, totalBooks: result.total_data })
                      resolve({
                        data: result.results,
                        page: query.page,
                        totalCount: result.count,
                      })
                      window.localStorage.setItem('current_book_page', query.page)
                    })
                })
            }

            editable={{
              onRowDelete: oldData =>
                new Promise((resolve, reject) => {
                  // setTimeout(() => {
                  const dataDelete = [...this.state.data];
                  const index = oldData.tableData.id;
                  console.log(index)
                  this.props.ondeleteBook(oldData.id, this.props.token)
                  console.log('err', this.props.error)
                  if (!this.props.error) {
                    dataDelete.splice(index, 1);
                    this.setState([...dataDelete]);
                  }
                  resolve();
                  // }, this.props.bookloading);
                })
            }}
            actions={[
              {
                icon: 'visibility_on',
                tooltip: 'Detail',
                onClick: (event, rowData) =>
                  !this.props.permissions || this.props.permissions.indexOf('book-detail') >= 0 ?
                    this.props.history.push(`/admin/book-detail/${rowData.id}`) :
                    this.setState({ isErrorDialogPopUp: true, hasErrorMessage: true, ErrorMessage: `You Are Not Authorized` })


              },
              {
                icon: "edit",
                tooltip: 'Edit',
                onClick: ((event, rowData) => {
                  if (rowData.number_of_books) {

                    this.props.getBookEntriesBatches(this.props.token, rowData.id,
                      (res) => {
                        console.log(typeof res)
                        if (typeof res === 'object') {
                          let batches = res.map(item => {
                            return item.batch
                          })
                          if (batches.length == 1) {
                            !this.props.permissions || this.props.permissions.indexOf('book-edit') >= 0 ?

                              this.props.history.push(`/admin/book-entries-edit/${rowData.id}`) :
                              this.setState({ isErrorDialogPopUp: true, hasErrorMessage: true, ErrorMessage: `You Are Not Authorized To Edit` })

                          } else {
                            this.setState({
                              isBatchListDialogOpen: true,
                              isEditBatch: true,
                              editBatchId: rowData.id,
                              batches: batches
                            })

                          }

                        }

                      },
                      () => {
                        this.setState({
                          isErrorDialogPopUp: true,
                          ErrorMessage: "Sorry, Something went wrong!"
                        })
                      }
                    )
                  } else {

                    this.setState({
                      isErrorDialogPopUp: true,
                      ErrorMessage: "Sorry, Book has no entries"
                    })
                  }
                  //
                })
              },
              {
                icon: "print",
                tooltip: 'Print Entries',
                onClick: ((event, rowData) => {
                  if (rowData.number_of_books) {
                    this.props.getBookEntriesBatches(this.props.token, rowData.id,
                      (res) => {
                        console.log(typeof res)
                        if (typeof res === 'object') {
                          let batches = res.map(item => {
                            return item.batch
                          })
                          if (batches.length == 1) {
                            this.props.printBatchCodes(batches, this.props.token,
                              () => {
                                this.setState({
                                  isPrintDialogOpen: true,
                                })
                              }, () => {
                                this.setState({
                                  isErrorDialogPopUp: true,
                                  ErrorMessage: "Sorry, Something went wrong!"
                                })
                              })
                          } else {
                            this.setState({
                              isBatchListDialogOpen: true,
                              batches: batches
                            })

                          }

                        }

                      },
                      () => {
                        this.setState({
                          isErrorDialogPopUp: true,
                          ErrorMessage: "Sorry, Something went wrong!"
                        })
                      }
                    )
                  } else {

                    this.setState({
                      isErrorDialogPopUp: true,
                      ErrorMessage: "Sorry, Book has no entries"
                    })
                  }
                  //
                })
              }

            ]}
            components={{
              Toolbar: props => (
                <div>
                  <div style={{ padding: 20, paddingBottom: 0 }}>
                    <Link label="Chip 1" color="secondary" style={{ marginRight: 5, float: 'right' }} to="/admin/bookcreate/" >
                      {this.props.permissions.length ?
                        !this.props.permissions || this.props.permissions.indexOf('book-add') >= 0 &&
                        <AddIcon style={{ fontSize: 40 }} /> :
                        <AddIcon style={{ fontSize: 40 }} />
                      }
                    </Link>
                  </div>
                  <MTableToolbar {...props} />

                </div>
              ),
              // OverlayLoading: props => (
              //   <div
              //     style={{
              //       position: "absolute",
              //       top: 0,
              //       left: 0,
              //       height: "100%",
              //       width: "100%",
              //       zIndex: 11,
              //     }}
              //   >
              //     <CustomSkeleton />
              //   </div>
              // )
            }}
            onRowClick={((evt, selectedRow) => this.setState({ selectedRow: selectedRow.tableData.id }))}
            options={{
              // filtering: true,
              actionsColumnIndex: -1,
              rowStyle: rowData => ({
                backgroundColor: (this.state.selectedRow === rowData.tableData.id) ? '#EEE' : '#FFF'
              }),
              emptyRowsWhenPaging: false,
              pageSize: 20,
              pageSizeOptions: [20, 50, 100]
            }}
          />
        </Fragment >
      </Hoc>
    )
  };
}
const mapStateToProps = state => {
  console.log(state)
  return {
    printHtml: state.book.printHtml,
    books: state.book.books,
    batches: state.book.batches,
    userId: state.auth.userId,
    editions: state.book.editions,
    categorys: state.book.categorys,
    subcategorys: state.book.subcategorys,
    token: state.auth.token,
    bookloading: state.book.loading,
    settingloading: state.setting.loading,
    error: state.book.error,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth,
    permissions: (typeof state.auth.user['permissions'] != 'undefined') ? state.auth.user.permissions : []
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onpostBook: (categoryName, token, userId) => dispatch(postBook(categoryName, token, userId)),
    ongetBook: (token) => dispatch(getBook(token)),
    ongetBookSubCategory: (token) => dispatch(getBookSubCategory(token)),
    ondeleteBook: (categoryId, token) => dispatch(deleteBook(categoryId, token)),
    oneditBook: (newCategory, categoryId, token, userId) => dispatch(editBook(newCategory, categoryId, token, userId)),
    ongetBookEdition: (token) => dispatch(getBookEdition(token)),
    ongetBookAuthor: (token) => dispatch(getBookAuthor(token)),
    authCheckState: () => dispatch(authCheckState()),
    getBookEntriesBatches: (token, bookId, onSuccess, onError) => dispatch(getBookEntryBatches(token, bookId, onSuccess, onError)),
    printBatchCodes: (batchNumber, token, onSuccess = null, onError = null) => dispatch(printBatchCodes(batchNumber, token, onSuccess, onError))

  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(BookCategory);