import React, { Fragment } from 'react';
import { WrapperSimple } from '../../../layout/Admin';
import MaterialTable from 'material-table';
import { PageTitle } from '../../../layout/Admin';
import { authCheckState } from '../../../store/actions/auth';
import axios from 'axios';
import { MTableToolbar } from 'material-table'
import { connect } from "react-redux";
import { Paper, Icon, Button, Select, Switch } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Hoc from '../../../hoc/hoc';
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import {
  Checkbox,
  Divider,
  Typography,
  TextField,
  MenuItem
} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { updateTrainingInfo, clearTrainingInfo } from '../../../store/actions/auth';
import { compose } from 'redux';
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Loading from '../../../components/Loading/index';
import BatchDialog from "../../../components/CustomDialog/BatchDialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { TablePagination } from '@material-ui/core';
import Snackbar from "@material-ui/core/Snackbar";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});





class Component extends React.Component {
  state = {
    data: [],
    training: '',
    formDisplay: false,
    loading: false,
    showPrintDialog: false,
    showEmailDialog: false,
    extramail: '',
    invalidMail: false,
    certificatetype: 0,
    checkList: {},
    trainingDialog: false,
    trainingId: null,
    trainings: [],
    showSnackBar: false,
    snackbarMessage: '',
  }
  componentDidMount() {
    this.loadData()
  }

  loadData = () => {
    this.props.authCheckState()
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    this.setState({ loading: true })
    axios
      .get(window.location.origin + `/api/v1/traninigsdetail/${this.props.match.params.id}/`)
      .then(res => {
        this.setState({ loading: false })
        let data = res.data
        console.log(data)
        this.setState({
          data: res.data.data,
          training: res.data.training,
          comittee: res.data.comittee

        })
        let trainingDate = {
          'comittee': res.data.comittee,
          'training': res.data.training
        }
        axios
          .get(window.location.origin + `/api/v1/trainee/trainings/`)
          .then(response => {
            let data = response.data
            if (typeof data === "object") {
              data = data.filter(each => each.name !== res.data.training)
              if (data.length) {
                this.setState({
                  trainingId: data[0].id,
                  trainings: data
                })
              }

            }


          })
          .catch(err => {
            this.setState({
              isErrorDialogPopUp: true,
              ErrorMessage: 'Failed to load trainings'
            })
          })

        this.props.updateTrainingInfo(trainingDate)
        // console.log(comittee)
        window.localStorage.setItem('trainingDate', JSON.stringify(trainingDate))


      }
      )
      .catch(err => {
        this.setState({ loading: false })

        console.log(err)
        this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })


      });

  }
  componentWillUnmount() {
    this.props.clearTrainingInfo()

  }


  validateTraineeCheck = () => {
    let checklist = Object.values(this.state.checkList)
    if (!checklist.filter(item => item).length) {
      this.setState({
        showSnackBar: true,
        snackbarMessage: 'कृपया प्रशिक्षार्थी छान्नुहोस् !'
      })
    }
    return checklist.filter(item => item).length
  }

  validateTrainingCheck = () => {
    if (!this.state.trainings.length) {
      this.setState({
        showSnackBar: true,
        snackbarMessage: 'कुनै प्रशिक्षण भेटिएन !'
      })
    }
    return this.state.trainings.length
  }

  showTrainingDialog = () => {
    if (this.validateTraineeCheck() && this.validateTrainingCheck()) {
      this.setState({
        trainingDialog: true
      })
    }
  }

  handleTrainingDialogClose = () => {
    this.setState({
      trainingDialog: false
    })
  }

  handleTrainingChange = (e) => {
    this.setState({
      trainingId: e.target.value
    })
  }


  updateTraining = () => {
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    this.setState({ loading: true })
    axios
      .post(window.location.origin + "/api/v1/trainee/movetrainees/", {
        trainingId: this.state.trainingId,
        checkList: this.state.checkList
      })
      .then(res => {
        this.setState({
          loading: false,
          trainingDialog: false,
          checkList: {}
        })
        this.loadData()
      }
      )
      .catch(err => {
        this.setState({ loading: false, trainingDialog: false, isErrorDialogPopUp: true, ErrorMessage: err.message })


      });
  }

  handleSnackClose = () => {
    this.setState({
      showSnackBar: false,
      snackbarMessage: ''

    })
  }


  render() {
    const handelAddClick = (e) => {
      e.preventDefault();
      this.setState({ formDisplay: true })
      console.log(this.state)
    }

    const handleEmailDialogClose = (e) => {
      this.setState({
        showEmailDialog: false
      })
    }
    const handleSendMail = (e) => {
      let isValid = true
      if (this.state.extramail != '') {
        this.state.extramail.split(',').map(each => {
          if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(each))) {

            isValid = isValid && false;
          }
        })
        if (!isValid) {

          this.setState({
            validateMail: 'Sorry, mail is not valid',
            invalidMail: true
          })
          return;
        } else {
          this.setState({
            validateMail: '',
            invalidMail: false
          })
        }
      }
      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      this.setState({
        loading: true,
        showEmailDialog: false,
      })
      axios
        .get(window.location.origin + `/api/v1/traninigsdetail/img_for_mail/?id=${this.props.match.params.id}&traineeId=${this.state.printId}${this.state.extramail == '' ? '' : '&email=' + this.state.extramail}`)
        .then(res => {
          this.setState({
            loading: false,
            showEmailDialog: false,
          })


        }
        )
        .catch(err => {
          this.setState({
            showEmailDialog: false,
            loading: false
          })

          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: "माफ गर्नुहोल केही त्रुती भएको छ।" })


        });
    }

    const handlePrintDialogClose = (e) => {
      this.setState({
        showPrintDialog: false
      })
    }
    const handlePrint = (e) => {

      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      this.setState({ loading: true })
      axios
        .get(window.location.origin + `/api/v1/traninigsdetail/certificate/?id=${this.props.match.params.id}&traineeId=${this.state.printId}${this.state.certificatetype ? '&p' : ''}`)
        .then(res => {
          let data = res.data
          console.log(data)

          let w = window.open()

          w.document.write(data)
          w.document.write('<scr' + 'ipt type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</sc' + 'ript>')

          w.document.close() // necessary for IE >= 10
          w.focus() //
          this.setState({
            loading: false,
            showPrintDialog: false,
          })

        }
        )
        .catch(err => {
          this.setState({
            showPrintDialog: false,
            loading: false
          })

          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: "माफ गर्नुहोल केही त्रुती भएको छ।" })


        });
    }
    const handleChangeCertificate = (e) => {
      this.setState({
        certificatetype: e.target.value
      })
    }
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };
    return (
      <Fragment>
        {this.state.loading ?

          <Loading /> :
          (
            <Hoc>
              <Snackbar
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                open={this.state.showSnackBar}
                autoHideDuration={2000}
                onClose={this.handleSnackClose}
                message={this.state.snackbarMessage}
                action={
                  <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleSnackClose}>
                      <CloseIcon fontSize="small" />
                    </IconButton>
                  </React.Fragment>
                }
              />

              <Dialog
                open={this.state.trainingDialog}
                onClose={this.handleTrainingDialogClose}
                TransitionComponent={Transition}
                aria-labelledby="draggable-dialog-title"
              >
                <DialogTitle>प्रशिक्षार्थीहरु सार्न प्रशिक्षण छान्नुहोस्</DialogTitle>
                <DialogContent>

                  <label htmlFor="demo-simple-select-label">प्रशिक्षण</label>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    style={{ marginLeft: '30px', width: 200 }}
                    value={this.state.trainingId}
                    onChange={this.handleTrainingChange}
                  >
                    {this.state.trainings.map(data => <MenuItem value={data.id}>{data.name}</MenuItem>)}
                  </Select>
                  <br />
                  <br />
                  <Typography variant="body1">
                    नोट: एक चोटि सारिएको प्रशिक्षार्थी पूर्ववत गर्न सकिदैन।
                  </Typography>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.updateTraining} color="primary" autoFocus>
                    Ok
                  </Button>
                  <Button onClick={this.handleTrainingDialogClose} color="primary" autoFocus>
                    Cancel
                  </Button>

                </DialogActions>
              </Dialog>
              <Dialog
                open={this.state.showPrintDialog}
                onClose={handlePrintDialogClose}
                TransitionComponent={Transition}
                aria-labelledby="draggable-dialog-title"
              >
                <DialogTitle>प्रमाण-पत्र / प्रशंसा-पत्र</DialogTitle>
                <DialogContent>
                  <Select
                    value={this.state.certificatetype}
                    onChange={handleChangeCertificate}
                  >
                    <MenuItem value={0}>प्रमाण-पत्र</MenuItem>
                    <MenuItem value={1}>प्रशंसा-पत्र</MenuItem>
                  </Select>
                </DialogContent>
                <DialogActions>
                  <Button onClick={handlePrint} color="primary" autoFocus>
                    Ok
                  </Button>
                  <Button onClick={handlePrintDialogClose} color="primary" autoFocus>
                    Cancel
                  </Button>
                </DialogActions>
              </Dialog>

              <Dialog
                open={this.state.showEmailDialog}
                onClose={handleEmailDialogClose}
                TransitionComponent={Transition}
                aria-labelledby="draggable-dialog-title"
              >
                <DialogTitle>प्रमाण-पत्र / प्रशंसा-पत्र</DialogTitle>
                <DialogContent>
                  <TextField
                    label="Extra Mail"
                    value={this.state.extramail}
                    helperText={this.state.invalidMail ? <a style={{ color: "red" }} >अवैध ईमेल</a> : ""}
                    onChange={e => { this.setState({ extramail: e.target.value }) }}
                  >
                  </TextField>
                </DialogContent>
                <DialogActions>
                  <Button onClick={handleSendMail} color="primary" autoFocus>
                    Ok
                  </Button>
                  <Button onClick={handleEmailDialogClose} color="primary" autoFocus>
                    Cancel
                  </Button>
                </DialogActions>
              </Dialog>

              <Dialog
                open={this.state.isErrorDialogPopUp}
                onClose={handleErrorClose}
                TransitionComponent={Transition}
                // PaperComponent={PaperComponent}
                aria-labelledby="draggable-dialog-title"
              >
                <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                  Error Message
              </DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={handleErrorClose} color="primary" autoFocus>
                    Ok
                </Button>
                </DialogActions>
              </Dialog>
              <MaterialTable
                title={
                  <div>

                    <div style={{ fontSize: 20, fontWeight: 'bold' }}>प्रशिक्षार्थि</div>
                    <div style={{ fontSize: 17, fontWeight: 'bold', marginTop: 5 }}>प्रशिक्षण नाम : {this.state.training && this.state.training}</div>
                  </div>
                }
                columns={[
                  {
                    title: '#',
                    field: 'include_in_self_registration',
                    render: rowData => {
                      var list = this.state.checkList
                      if (list[rowData.id] === undefined) {
                        list[rowData.id] = false
                      }
                      this.state.checkList = list;
                      return <Checkbox
                        checked={this.state.checkList[rowData.id]}
                        onChange={(e) => {
                          var list = this.state.checkList
                          if (list[rowData.id] === undefined) {
                            list[rowData.id] = false
                          }
                          list[rowData.id] = e.target.checked
                          this.setState({
                            checkList: list
                          })
                        }}
                        name="checkedA"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    },
                    export: false
                  },
                  {
                    title: `परिचय नम्बर`,
                    field: 'serial_number',
                    emptyValue: "",
                    defaultSort: 'desc'
                  },
                  {
                    title: 'नाम',
                    field: 'name',
                    emptyValue: "",
                    render: rowData => <a>{rowData.name.length >= 25 ? `${rowData.name.substr(0, 25)}...` : rowData.name}</a>

                  },
                  {
                    title: 'पद',
                    field: 'post',
                    emptyValue: "",
                  },
                  {
                    title: 'ठेगाना',
                    field: 'address',
                    emptyValue: "",
                  },
                  {
                    title: 'उमेर',
                    field: 'age',
                    emptyValue: "",
                  },
                  {
                    title: 'प्रशिक्षार्थी संस्था',
                    field: 'trainee_institution',
                    emptyValue: "",
                  },
                  {
                    title: 'जिल्ला',
                    field: 'district',
                    emptyValue: "",
                  },
                  {
                    title: 'वार्ड नम्बर',
                    field: 'ward_number',
                    emptyValue: "",
                  },
                  {
                    title: 'ईमेल',
                    field: 'email',
                    emptyValue: "",
                  },
                  {
                    title: 'समावेशिता',
                    field: 'inclusion',
                    emptyValue: "",
                  },
                  {
                    title: 'लिंग',
                    field: 'gender',
                    emptyValue: "",
                  },
                  {
                    title: 'शैक्षिक योग्यता',
                    field: 'educational_qualification',
                    emptyValue: "",
                  },
                  {
                    title: 'सम्पर्क नम्बर',
                    field: 'phone_number',
                    emptyValue: "",
                  }
                ]}
                data={this.state.data}
                options={
                  {
                    exportButton: true,
                    exportFileName: "प्रशिक्षार्थि",
                    // filtering: true,
                    actionsColumnIndex: -1,
                    emptyRowsWhenPaging: false,
                    pageSize: 100,
                    pageSizeOptions: [20, 50, 100]
                  }
                }
                editable={{
                  onRowDelete: oldData =>
                    new Promise((resolve, reject) => {
                      const dataDelete = [...this.state.data];
                      const index = oldData.tableData.id;
                      this.setState({ loading: true })

                      axios
                        .delete(window.location.origin + `/api/v1/trainee/${oldData.id}/`)
                        .then(res => {
                          axios
                            .get(window.location.origin + "/api/v1/trainee/")
                            .then(res => {
                              this.setState({ loading: false })

                              let data = res.data
                              console.log(data)
                              this.setState({
                                data: res.data
                              })
                            }
                            )
                            .catch(err => {
                              this.setState({ loading: false })

                              console.log(err)
                              this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

                            });
                          dataDelete.splice(index, 1);
                          this.setState([...dataDelete]);
                        }
                        )
                        .catch(err => {
                          this.setState({ loading: false })

                          console.log(err)
                          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

                        })
                      console.log(index)
                      resolve();
                    })
                }}
                // add condition to show print or not
                actions={true ? [

                  {
                    icon: 'edit',
                    tooltip: 'Edit',
                    onClick: (event, rowData) =>
                      this.props.history.push(`/admin/training/${this.props.match.params.id}/trainee-edit/${rowData.id}`)


                  },
                  {
                    icon: 'print',
                    tooltip: 'Print',
                    onClick: (event, rowData) => {
                      this.setState({
                        printId: rowData.id,
                        showPrintDialog: true
                      })
                    }
                  },
                  {
                    icon: 'email',
                    tooltip: 'Email',
                    onClick: (event, rowData) => {
                      this.setState({
                        printId: rowData.id,
                        showEmailDialog: true
                      })
                    }
                  }
                ] : [

                    {
                      icon: 'edit',
                      tooltip: 'Edit',
                      onClick: (event, rowData) =>
                        this.props.history.push(`/admin/training/${this.props.match.params.id}/trainee-edit/${rowData.id}`)


                    },
                  ]}
                localization={{
                  toolbar: {
                    searchTooltip: 'खोजी गर्नुहोस्',
                    searchPlaceholder: 'खोजी गर्नुहोस्'
                  },
                  header: {
                    actions: 'कार्यहरू'
                  },
                  body: {
                    emptyDataSourceMessage: 'कुनै रेकर्ड छैन',
                    filterRow: {
                      filterTooltip: 'Filter'
                    }
                  }
                }}
                components={{
                  // Pagination: props => (

                  //   <TablePagination
                  //     {...props}
                  //     rowsPerPageOptions={[5, 10, 20, 30]}
                  //     rowsPerPage={this.state.numberRowPerPage}
                  //     count={this.state.totalRow}
                  //     page={
                  //       true
                  //         ? this.state.pageNumber
                  //         : this.state.pageNumber - 1
                  //     }
                  //     onChangePage={(e, page) =>
                  //       this.handleChangePage(page + 1)
                  //     }
                  //     onChangeRowsPerPage={event => {
                  //       props.onChangeRowsPerPage(event);
                  //       this.handleChangeRowPerPage(event.target.value);
                  //     }}
                  //   />
                  // ),
                  Toolbar: props => (
                    <div>
                      <div style={{ padding: 20, paddingBottom: 0 }}>
                        <Link label="Chip 1" color="secondary" style={{ marginRight: 5, float: 'right' }} to={`/admin/training/${this.props.match.params.id}/trainee-create`} >
                          {this.props.permissions.length ?
                            !this.props.permissions || this.props.permissions.indexOf('edition-add') >= 0 &&
                            <AddIcon style={{ fontSize: 40 }} /> :
                            <AddIcon style={{ fontSize: 40 }} />
                          }
                        </Link>
                      </div>
                      <MTableToolbar {...props} />

                    </div>
                  ),
                }}
              />
            </Hoc>

          )
        }
        <Button
          variant="contained" color="primary"
          onClick={this.showTrainingDialog}
        >

          प्रशिक्षार्थी सार्नुहोस्
        </Button>


      </Fragment >
    )
  };
}
const mapStateToProps = state => {
  console.log(state.book.categorys)
  return {
    editions: state.book.editions,
    token: state.auth.token,
    loading: state.book.loading,
    error: state.book.error,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth,
    permissions: (typeof state.auth.user['permissions'] != 'undefined') ? state.auth.user.permissions : []
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateTrainingInfo: (data) => dispatch(updateTrainingInfo(data)),
    clearTrainingInfo: () => dispatch(clearTrainingInfo()),
    authCheckState: () => dispatch(authCheckState()),

  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component);
