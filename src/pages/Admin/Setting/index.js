import React, { Fragment } from 'react';
import { connect } from "react-redux";
import { authCheckState } from '../../../store/actions/auth';
import { compose } from 'redux';
import {
  withStyles,
} from '@material-ui/core/styles';
import { getSetting, postSetting } from "../../../store/settings/action";
import './index.css'
import Grid from '@material-ui/core/Grid';
import { SwatchesPicker } from 'react-color';
import LinearProgress from '@material-ui/core/LinearProgress';
import { SmileOutlined } from '@ant-design/icons';

import {
  Form,
  Input,
  Button,
  Radio,
  Select,
  Cascader,
  DatePicker,
  InputNumber,
  TreeSelect,
  Spin,
  Switch,
  Row,
   Col,
    Divider,
} from 'antd';

// import default style

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100ch'
    },
    input: {
      display: 'none',
    },
  },
})







class Setting extends React.Component {
  state = {
    data:{},
    loading:false,
    submitLoading:false,
  }

  componentDidMount() {
    this.state.loading=true
    this.setState({loading:true})
    this.props.getSetting(this.props.token,(res)=>{
     this.setState({data:res.data})
     console.log(this.state.data,'saurav thakur')
    this.setState({loading:false})

    },
    (err)=>{
    this.setState({loading:false})
      console.log(err,'saurav thakur')
    })
  }


  render() {
    const { classes } = this.props;
    const sighandleImageChange = (e) => {
      if (e.target.files[0]) {
        this.setState({
          sigimage: e.target.files[0],
          sigimageUrl: URL.createObjectURL(e.target.files[0]),
        })
      }
    }
    const loghandleImageChange = (e) => {
      if (e.target.files[0]) {
        this.setState({
          logimage: e.target.files[0],
          logimageUrl: URL.createObjectURL(e.target.files[0]),
        })
      }
    }

// const layout = {
//   labelCol: { span: 10 },
//   wrapperCol: { span: 100 },
// };

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};

   const onFinish = (values) => {
     this.setState({submitLoading:true})
    this.props.postSetting(values, this.props.token,
          () => {
            //success
           this.setState({submitLoading:false})
            
          }, () => {
            //error
            
           this.setState({submitLoading:false})
          })
  };
    const testData={'data':this.state.data}

    return (
    <Fragment>
      {this.state.loading?
      <div>
      <LinearProgress color="secondary" />
      </div>
      :
      <div className="sub-container">
      
    <Form 
    scrollToFirstError
    name="nest-messages"
    onFinish={onFinish} 
    layout= 'vertical'
    initialValues={{
      name:this.state.data.name,
      email:this.state.data.email,
      phone_number:this.state.data.phone_number,
      address:this.state.data.address,
      navbar_color:this.state.data.navbar_color,
      sidebar_color:this.state.data.sidebar_color,
      introduction:this.state.data.introduction,
      }}
    validateMessages={validateMessages}>
  <Divider orientation="left">Setting</Divider>
    <Grid container spacing={2}>
    <Grid item xm={12} xl={8} md={8}>
    <Grid container spacing={3}>
    
    <Grid item  xm={12} xl={3} md={3} >
      <Form.Item  
      name= 'name'
      label="Name" 
      rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      </Grid>
      <Grid item  xm={12} xl={3} md={3} >
      <Form.Item 
      name='email' 
      label="Email" 
      rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      </Grid>
      <Grid item  xm={12} xl={3} md={3} >
      <Form.Item 
      name='phone_number' 
      label="Phone Number" 
      rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      </Grid>
      <Grid item  xm={12} xl={3} md={3} >
      <Form.Item 
      name='address' 
      label="Address" 
      rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      </Grid>
      </Grid>
  <Divider orientation="left"></Divider>
  <Grid container>
      <Grid item  xm={12} xl={3} md={3} >


      <Form.Item name='introduction' label="Introduction">
        <Input.TextArea />
      </Form.Item>
    </Grid>
    </Grid>
  
    </Grid>
    <Grid item   xm={12} xl={4} md={4}>

  <Grid container spacing={3}>
    
      {/* <Grid item  xm={12} xl={12} md={12} >
      
        <Form.Item  
        name={`navbar_color  ${this.state.data.navbar_color}`} 
        label={<span style={{color:`${this.state.data.navbar_color} `}}>NavBar Color</span>} 
        >
          <SwatchesPicker name='navbar_color'   />
        </Form.Item>
      </Grid>
        <Grid item  xm={12} xl={12} md={12} >
        <Form.Item  
       name={`sidebar_color  ${this.state.data.sidebar_color}`} 
        label={<span style={{color:`${this.state.data.sidebar_color} `}}>SideBar Color</span>}
        >
          <SwatchesPicker  />
        </Form.Item>
        </Grid>
      */}
    </Grid>
    </Grid>

    </Grid>
  <Divider orientation="left"></Divider>

    
      {/* <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}> */}
      <Form.Item  wrapperCol={{  offset: 0 }}>
        <Button type="primary" danger={this.state.submitLoading} loading={this.state.submitLoading} type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  </div>
    }
  </Fragment>
    )
  }

}



const mapStateToProps = state => {
  let settings = state.setting.settings
  console.log(settings)
  return {
    error: state.setting.error,
    loading: state.setting.loading,
    token: state.auth.token,
    setting: state.setting.setting,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getSetting: (token,onSuccess , onError ) => dispatch(getSetting(token,onSuccess , onError)),
    postSetting: (data, token, onSuccess, onError) => dispatch(postSetting(data, token, onSuccess, onError)),
    authCheckState: () => dispatch(authCheckState())
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Setting);
