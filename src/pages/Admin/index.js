export { default as DashboardDefault } from './Dashboard';
export { default as Buttons } from './Setting';
export { default as Badges } from './Setting';
export { default as LogInPage } from './LoginPage';
export { default as SignUpPage } from './SignupPage ';
export { default as trainee } from './trainee/index';
export { default as trainee_ce } from './trainee/created_edit';