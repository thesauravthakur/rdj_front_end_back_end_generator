import React, { Fragment } from 'react';

import { PageTitle } from '../../../layout/Admin';

import Dashboard from '../../../components/Dashboard';

export default function DashboardDefault() {
  return (
    <Fragment>
      <Dashboard />
    </Fragment>
  );
}
