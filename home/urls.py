from django.contrib import admin
from django.urls import path, include, re_path
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static
from . import api

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
# from downloads.views import  DownloadViewSet
urlpatterns = [
    path('api-auth/', include('rest_framework.urls',namespace='rest_framework')),
    path('api/user/', include('users.urls',namespace='users')),
    # path('rest-auth/', include('rest_auth.urls')),
    path('api/v1/', include(api.router.urls)),
    # path('api/downloads', DownloadViewSet),
    # path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += [
        re_path(r'^.*', TemplateView.as_view(template_name='index.html')),
        # path('/', TemplateView.as_view(template_name='index.html')),
    ]
