import os
BASE_DIR=''
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
MODEL_NAME = input('Enter the app name:').lower()
# APP_DIR = os.path.join(BASE_DIR, f'{MODEL_NAME}')
HOME_DIR = os.path.join(BASE_DIR, 'home')
SETTING_PATH=os.path.join(HOME_DIR, 'settings/base.py')
API_PATH=os.path.join(HOME_DIR, 'api.py')
STORE_DIR=os.path.join(BASE_DIR, 'src/store')
ACTION_TYPE_PATH=os.path.join(STORE_DIR,f'{MODEL_NAME}/actionType.js')
REDUCER_PATH=os.path.join(STORE_DIR,f'{MODEL_NAME}/reducer.js')
ACTION_PATH=os.path.join(STORE_DIR,f'{MODEL_NAME}/action.js')
INDEX_PATH=os.path.join(BASE_DIR, 'src/index.js')



ACTION_TYPE_CONTENT_POST_ARRAY=[
    'POST_START',
    'POST_SUCCESS',
    'POST_FAIL',
]

ACTION_TYPE_CONTENT_GET_ARRAY=[
    'GET_START',
    'GET_SUCCESS',
    'GET_FAIL',
]

ACTION_TYPE_CONTENT_GET_DETAIL_ARRAY=[
    'DETAIL_START',
    'DETAIL_SUCCESS',
    'DETAIL_FAIL',
]

ACTION_TYPE_CONTENT_EDIT_ARRAY=[
    'EDIT_START',
    'EDIT_SUCCESS',
    'EDIT_FAIL',
]


ACTION_TYPE_CONTENT_DELETE_ARRAY=[
    'DELETE_START',
    'DELETE_SUCCESS',
    'DELETE_FAIL',
]


def actionTypeArrayOperation(actionType,file):
    for item in actionType:
            file.write(f'export const {MODEL_NAME.upper()}_{item} = "{MODEL_NAME.upper()}_{item}"\n')
    file.write('\n\n')




def createActionTypeFile():
    if os.path.exists(os.path.join(STORE_DIR,MODEL_NAME)):
        action_type_file_open = open(ACTION_TYPE_PATH, 'at')

        actionTypeArrayOperation(ACTION_TYPE_CONTENT_POST_ARRAY,action_type_file_open)
        actionTypeArrayOperation(ACTION_TYPE_CONTENT_GET_ARRAY,action_type_file_open)
        actionTypeArrayOperation(ACTION_TYPE_CONTENT_GET_DETAIL_ARRAY,action_type_file_open)
        actionTypeArrayOperation(ACTION_TYPE_CONTENT_EDIT_ARRAY,action_type_file_open)
        actionTypeArrayOperation(ACTION_TYPE_CONTENT_DELETE_ARRAY,action_type_file_open)
    else:
        print(f'=> {MODEL_NAME} Does Not Exits (Throw Back From Action Type Function)')




def reducerFunctionalOperation(MODEL_NAME,reducer_file_open,action_type):
    loading='false'
    error='null'
    success=False
    detailText=''

    if action_type.split('_')[1]=='START':
        loading='true'
    elif action_type.split('_')[1]=='SUCCESS':
        loading='false'
        success=True
    elif action_type.split('_')[1]=='FAIL':
        loading='false'
        error='action.error'
    
    if action_type.split('_')[0] != 'GET':
        detailText='Detail'

    reducer_file_open.write(f'const {MODEL_NAME}{action_type.split("_")[0].lower().capitalize()}{action_type.split("_")[1].lower().capitalize()} = (state, action) => ')
    reducer_file_open.write('{\n')
    reducer_file_open.write('\treturn updateObject(state, {\n')
    reducer_file_open.write(f'\t\terror: {error},\n')
    reducer_file_open.write(f'\t\tloading: {loading},\n')
    if success:
        reducer_file_open.write(f'\t\t{MODEL_NAME}{detailText}: action.{MODEL_NAME}{detailText},\n')
    reducer_file_open.write('\t});\n')
    reducer_file_open.write('};\n')





def createReducerFile():
    if os.path.exists(os.path.join(STORE_DIR,MODEL_NAME)):
        reducer_file_open = open(REDUCER_PATH, 'at')
        reducer_file_open.write('import * as actionTypes from "./actionType";\n')
        reducer_file_open.write('import { updateObject } from "../utility";\n')
        reducer_file_open.write('\n\nexport const initialState = {\n')
        reducer_file_open.write('\terror: null,\n')
        reducer_file_open.write('\tloading: null,\n')
        reducer_file_open.write(f"\t{MODEL_NAME}:[],\n")
        reducer_file_open.write(f"\t{MODEL_NAME}Detail:null,\n")
        reducer_file_open.write('}\n')
        reducer_file_open.write('\n\n\n\n\n\n')

        for item in ACTION_TYPE_CONTENT_GET_ARRAY:
            reducerFunctionalOperation(MODEL_NAME,reducer_file_open,item)
        
        reducer_file_open.write('\n\n\n\n\n\n')


        for item in ACTION_TYPE_CONTENT_POST_ARRAY:
            reducerFunctionalOperation(MODEL_NAME,reducer_file_open,item)
        
        reducer_file_open.write('\n\n\n\n\n\n')

        
        for item in ACTION_TYPE_CONTENT_EDIT_ARRAY:
            reducerFunctionalOperation(MODEL_NAME,reducer_file_open,item)
        
        reducer_file_open.write('\n\n\n\n\n\n')

        
        for item in ACTION_TYPE_CONTENT_GET_DETAIL_ARRAY:
            reducerFunctionalOperation(MODEL_NAME,reducer_file_open,item)
        
        reducer_file_open.write('\n\n\n\n\n\n')

        
        for item in ACTION_TYPE_CONTENT_DELETE_ARRAY:
            reducerFunctionalOperation(MODEL_NAME,reducer_file_open,item)
        
        reducer_file_open.write('\n\n\n\n\n\n')


        reducer_file_open.write("const reducer = (state = initialState, action) => { \n")
        reducer_file_open.write("\tswitch (action.type) { \n")
        

        for item in ACTION_TYPE_CONTENT_GET_ARRAY:
            reducer_file_open.write(f"\t\tcase actionTypes.{MODEL_NAME.upper()}_{item}:\n")
            reducer_file_open.write(f"\t\t\treturn {MODEL_NAME}{item.split('_')[0].lower().capitalize()}{item.split('_')[1].lower().capitalize()}(state, action);\n")
        reducer_file_open.write('\n\n\n\n\n\n')
        
        for item in ACTION_TYPE_CONTENT_POST_ARRAY:
            reducer_file_open.write(f"\t\tcase actionTypes.{MODEL_NAME.upper()}_{item}:\n")
            reducer_file_open.write(f"\t\t\treturn {MODEL_NAME}{item.split('_')[0].lower().capitalize()}{item.split('_')[1].lower().capitalize()}(state, action);\n")
        reducer_file_open.write('\n\n\n\n\n\n')

        for item in ACTION_TYPE_CONTENT_EDIT_ARRAY:
            reducer_file_open.write(f"\t\tcase actionTypes.{MODEL_NAME.upper()}_{item}:\n")
            reducer_file_open.write(f"\t\t\treturn {MODEL_NAME}{item.split('_')[0].lower().capitalize()}{item.split('_')[1].lower().capitalize()}(state, action);\n")
        reducer_file_open.write('\n\n\n\n\n\n')

        for item in ACTION_TYPE_CONTENT_GET_DETAIL_ARRAY:
            reducer_file_open.write(f"\t\tcase actionTypes.{MODEL_NAME.upper()}_{item}:\n")
            reducer_file_open.write(f"\t\t\treturn {MODEL_NAME}{item.split('_')[0].lower().capitalize()}{item.split('_')[1].lower().capitalize()}(state, action);\n")
        reducer_file_open.write('\n\n\n\n\n\n')

        for item in ACTION_TYPE_CONTENT_DELETE_ARRAY:
            reducer_file_open.write(f"\t\tcase actionTypes.{MODEL_NAME.upper()}_{item}:\n")
            reducer_file_open.write(f"\t\t\treturn {MODEL_NAME}{item.split('_')[0].lower().capitalize()}{item.split('_')[1].lower().capitalize()}(state, action);\n")
        reducer_file_open.write('\n\n\n')

        reducer_file_open.write("\t\tdefault:\n")
        reducer_file_open.write("\t\t\treturn state;\n")
        reducer_file_open.write("\t}\n")
        reducer_file_open.write("};\n")
        reducer_file_open.write("export default reducer;\n")













        

    else:
        print(f'{MODEL_NAME} Does Not Exits (Throw Back From Reducer Function)')



def actionOperation(model_name,action_type,action_file_open):
    if action_type.split('_')[1]=='START':
        action_file_open.write('export const ')
        action_file_open.write(f'{model_name}{action_type.split("_")[0].lower().capitalize()}{action_type.split("_")[1].lower().capitalize()}')
        action_file_open.write('= () => {\n')
        action_file_open.write('\treturn {\n')
        action_file_open.write(f'\t\ttype: actionTypes.{model_name.upper()}_{action_type}\n')
        action_file_open.write('\t\t};\n};\n')
        action_file_open.write('\n\n')

    elif action_type.split('_')[1]=='SUCCESS':
        detail='Detail'
        if action_type.split('_')[0] == 'GET':
            detail=''

        action_file_open.write('export const ')
        action_file_open.write(f'{model_name}{action_type.split("_")[0].lower().capitalize()}{action_type.split("_")[1].lower().capitalize()}= ({model_name}{detail}) =>')
        action_file_open.write(' {\n')
        action_file_open.write('\treturn {\n')
        action_file_open.write(f'\t\ttype: actionTypes.{model_name.upper()}_{action_type},\n')
        action_file_open.write(f'\t\t {model_name}{detail}: {model_name}{detail}\n')
        action_file_open.write('\t\t};\n};\n')
        action_file_open.write('\n\n')
    
    elif action_type.split('_')[1]=='FAIL':
        action_file_open.write('export const ')
        action_file_open.write(f'{model_name}{action_type.split("_")[0].lower().capitalize()}{action_type.split("_")[1].lower().capitalize()}')
        action_file_open.write('= (error) => {\n')
        action_file_open.write('\treturn {\n')
        action_file_open.write(f'\t\ttype: actionTypes.{model_name.upper()}_{action_type},\n')
        action_file_open.write(f'\t\t error: error\n')
        action_file_open.write('\t\t};\n};\n')
        action_file_open.write('\n\n')
    

def actionfunctionalOperation(model_name,action_type_array,action_file_open):
    action_type=action_type_array[0]
    export=''
    postType=''
    url=''
    if action_type.split('_')[0] =='POST':
        export=f'export const post{model_name} = ({model_name},token, onSuccess = null, onError = null) => '
        postType='post'
        url=f'\t\t\t .{postType}(mainLink + `/api/v1/{model_name}/`,{model_name}) \n'
    elif action_type.split('_')[0] =='GET':
        export=f'export const get{model_name} = (token, onSuccess = null, onError = null) => '
        postType='get'
        url=f'\t\t\t .{postType}(mainLink + `/api/v1/{model_name}/`) \n'
    
    elif action_type.split('_')[0] =='DETAIL':
        export=f'export const getDetail{model_name} = (token,id, onSuccess = null, onError = null) => '
        postType='get'
        id='${id}'
        url=f'\t\t\t .{postType}(mainLink + `/api/v1/{model_name}/{id}/`) \n'
    
    elif action_type.split('_')[0] =='EDIT':
        export=f'export const edit{model_name} = ({model_name},token,id, onSuccess = null, onError = null) => '
        postType='put'
        id='${id}'
        url=f'\t\t\t .{postType}(mainLink + `/api/v1/{model_name}/{id}/`,{model_name}) \n'
    
    elif action_type.split('_')[0] =='DELETE':
        export=f'export const delete{model_name} = (token,id, onSuccess = null, onError = null) => '
        postType='delete'
        id='${id}'
        url=f'\t\t\t .{postType}(mainLink + `/api/v1/{model_name}/{id}/`) \n'

    action_file_open.write(export)
    action_file_open.write('{\n')
    action_file_open.write('\treturn dispatch => {\n')
    action_file_open.write(f'\t\tdispatch({model_name}{action_type.split("_")[0].lower().capitalize()}Start());\n')
    action_file_open.write('\t\taxios.defaults.headers = {\n')
    action_file_open.write('\t\t\t "Content-Type": "application/json",\n\t\t\t Authorization: `Token ${token}`,\n\t\t};\n')
    action_file_open.write('\t\taxios.\n')
    action_file_open.write(url)
    action_file_open.write('\t\t\t .then(res => {\n')
    action_file_open.write(f'\t\t\t\t dispatch({model_name}{action_type.split("_")[0].lower().capitalize()}Success(res.data));\n')
    action_file_open.write("\t\t\t\t if (typeof onSuccess == 'function') {\n\t\t\t\t\tonSuccess()\n\t\t\t\t}\n")

    action_file_open.write('\t\t\t}\n')
    action_file_open.write('\t\t\t ).catch(err => {\n')
    action_file_open.write(f'\t\t\t\t dispatch({model_name}{action_type.split("_")[0].lower().capitalize()}Fail(err));\n')
    action_file_open.write("\t\t\t\t if (typeof onError == 'function') {\n\t\t\t\t\tonError()\n\t\t\t\t}\n")
    action_file_open.write('\t\t\t });\n')
    action_file_open.write('\t };\n')
    action_file_open.write(' };\n')
    action_file_open.write('\n\n\n\n\n\n\n\n\n')




def createActionFile():
    if os.path.exists(os.path.join(STORE_DIR,MODEL_NAME)):
        action_file_open = open(ACTION_PATH, 'at')
        action_file_open.write('import axios from "axios";\n')
        action_file_open.write('import * as actionTypes from "./actionType";\n')
        action_file_open.write('import { mainLink } from "../utils";\n\n\n\n')

        for action_type in ACTION_TYPE_CONTENT_POST_ARRAY:
            actionOperation(MODEL_NAME,action_type,action_file_open)
        action_file_open.write('\n\n\n\n\n\n\n')
        for action_type in ACTION_TYPE_CONTENT_GET_ARRAY:
            actionOperation(MODEL_NAME,action_type,action_file_open)
        action_file_open.write('\n\n\n\n\n\n\n')
        for action_type in ACTION_TYPE_CONTENT_EDIT_ARRAY:
            actionOperation(MODEL_NAME,action_type,action_file_open)
        action_file_open.write('\n\n\n\n\n\n\n')
        for action_type in ACTION_TYPE_CONTENT_DELETE_ARRAY:
            actionOperation(MODEL_NAME,action_type,action_file_open)
        action_file_open.write('\n\n\n\n\n\n\n')
        for action_type in ACTION_TYPE_CONTENT_GET_DETAIL_ARRAY:
            actionOperation(MODEL_NAME,action_type,action_file_open)
        action_file_open.write('\n\n\n\n\n\n\n')


        actionfunctionalOperation(MODEL_NAME,ACTION_TYPE_CONTENT_POST_ARRAY,action_file_open)
        actionfunctionalOperation(MODEL_NAME,ACTION_TYPE_CONTENT_GET_ARRAY,action_file_open)
        actionfunctionalOperation(MODEL_NAME,ACTION_TYPE_CONTENT_EDIT_ARRAY,action_file_open)
        actionfunctionalOperation(MODEL_NAME,ACTION_TYPE_CONTENT_GET_DETAIL_ARRAY,action_file_open)
        actionfunctionalOperation(MODEL_NAME,ACTION_TYPE_CONTENT_DELETE_ARRAY,action_file_open)


    
    else:
        print(f'{MODEL_NAME} Does Not Exits (Throw Back From Action Function)')






def createAppFolder():
    if os.path.exists(STORE_DIR):
        if not os.path.exists(os.path.join(STORE_DIR,MODEL_NAME)):
            os.mkdir(os.path.join(STORE_DIR,MODEL_NAME))


# INDEX_PATH
def addModelToIndexFile():
    if os.path.exists(INDEX_PATH):
        index_file_open = open(INDEX_PATH, 'r')
        index_file_data = index_file_open.readlines()
        index_file_data[0]=f'import React from "react";\nimport {MODEL_NAME}Reducer from "./store/{MODEL_NAME}/reducer"\n'
        base_file = open(INDEX_PATH, 'w')
        base_file.writelines(index_file_data)
        base_file.close()


        index_file_open = open(INDEX_PATH, 'r')
        index_file_data = index_file_open.readlines()
        for i in range(len(index_file_data)):
            if index_file_data[i] =='const rootReducer = combineReducers({\n':
                index_file_data[i-1]='}\nconst rootReducer = combineReducers({\n'
                index_file_data[i]=f'\t{MODEL_NAME}:{MODEL_NAME}Reducer,\n'
                base_file = open(INDEX_PATH, 'w')
                base_file.writelines(index_file_data)
                base_file.close()

        index_file_open = open(INDEX_PATH, 'r')
        index_file_data = index_file_open.readlines()
        for i in range(len(index_file_data)):
            if  'whitelist' in   index_file_data[i]:
                index_file_data[i]=f"{index_file_data[i].split(']')[0]},'{MODEL_NAME}']\n"
                base_file = open(INDEX_PATH, 'w')
                base_file.writelines(index_file_data)
                base_file.close()







def main():
    createAppFolder()
    createActionTypeFile()
    createActionFile()
    createReducerFile()
    addModelToIndexFile()

if __name__ == "__main__":
    main()
