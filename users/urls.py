from rest_framework.routers import DefaultRouter
from .views import UserViewSets, CustomUserCreate,BlacklistTokenUpdateView
from django.urls import path, include, re_path
app_name='users'

urlpatterns=[
    path('register/',CustomUserCreate.as_view(),name='create_user'),
    path('logout/blacklist/', BlacklistTokenUpdateView.as_view(),
         name='blacklist')
]