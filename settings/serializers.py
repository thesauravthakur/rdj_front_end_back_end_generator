from rest_framework import serializers
from rest_framework.authtoken.models import Token
from . import models


class SettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Setting
        fields = "__all__"
