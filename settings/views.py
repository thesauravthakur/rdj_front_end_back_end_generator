from django.shortcuts import render
from rest_framework import viewsets
from . import models
from django.db import transaction
from . import serializers
from rest_framework import status
from rest_framework.response import Response
from rest_framework import decorators
from PIL import Image
from io import BytesIO
import shutil
import sys
import os
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
import json

def is_jsonable(x):
	try:
		return json.dumps(x)
	except:
		return str(x)

class SettingViewSets(viewsets.ModelViewSet):
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]
    serializer_class = serializers.SettingSerializer
    queryset = models.Setting.objects.all()

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        print(request.data)
        data=request.data
        dataKeys=request.data.keys()
        for key in dataKeys:
            if models.Setting.objects.filter(key=key).exists():
                model=models.Setting.objects.filter(key=key)[0]
                model.key=key
                model.value=is_jsonable(data[key])
                model.save()
                print('updated')
            else:
                model=models.Setting()
                model.key=key
                model.value=is_jsonable(data[key])
                model.save()
                print('created')
        return Response(status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):
        print('saurav')
        queryset=models.Setting.objects.all()
        dataToSend={}
        for item in queryset:
            print(item.key,item.value)
            dataToSend[item.key]=json.loads(item.value)
        return Response(data=dataToSend,status=status.HTTP_201_CREATED)
        


    # @transaction.atomic
    # def create(self, request, *args, **kwargs):
    #     print(request.data)
    #     data = request.data
    #     uploadedImagesig = data['principal_signature']
    #     uploadedImagelogo = data['logo']
    #     if models.Setting.objects.all().exists():
    #         setting = models.Setting.objects.all()[0]
    #         if (uploadedImagesig) and not isinstance(uploadedImagesig, str):
    #             imageTemproary = Image.open(uploadedImagesig)
    #             # imageTemproary = imageTemproary.convert('RGB')
    #             outputIoStream = BytesIO()
    #             # imageTemproaryResized = imageTemproary.resize((600, 600))
    #             imageTemproary.save(outputIoStream, format='PNG', quality=100)
    #             outputIoStream.seek(0)
    #             uploadedImagesig = InMemoryUploadedFile(
    #                 outputIoStream, 'ImageField',
    #                 "%s.jpg" % uploadedImagesig.name.split('.')[0],
    #                 'image/png', sys.getsizeof(outputIoStream), None)
    #             folder_path = os.path.join(settings.MEDIA_ROOT, 'signature')
    #             old_folder_path = os.path.join(
    #                 settings.MEDIA_ROOT, str(setting.principal_signature))
    #             if os.path.exists(old_folder_path):
    #                 print(setting.principal_signature)
    #                 print(True)
    #                 os.remove(old_folder_path)
    #             setting.principal_signature = uploadedImagesig
    #         if (uploadedImagelogo) and not isinstance(uploadedImagelogo, str):
    #             imageTemproary = Image.open(uploadedImagelogo)
    #             # imageTemproary = imageTemproary.convert('RGB')
    #             outputIoStream = BytesIO()
    #             # imageTemproaryResized = imageTemproary.resize((600, 600))
    #             imageTemproary.save(outputIoStream, format='PNG', quality=100)
    #             outputIoStream.seek(0)
    #             uploadedImagelogo = InMemoryUploadedFile(
    #                 outputIoStream, 'ImageField',
    #                 "%s.jpg" % uploadedImagelogo.name.split('.')[0],
    #                 'image/png', sys.getsizeof(outputIoStream), None)
    #             folder_path = os.path.join(settings.MEDIA_ROOT, 'signature')
    #             old_folder_path = os.path.join(settings.MEDIA_ROOT,
    #                                            str(setting.logo))
    #             if os.path.exists(old_folder_path):
    #                 print(setting.logo)
    #                 print(True)
    #                 os.remove(old_folder_path)
    #             setting.logo = uploadedImagelogo
    #         setting.name = data['name']
    #         setting.address = data['address']
    #         # setting.principal_signature = data['principal_signature']
    #         # setting.logo = data['logo']
    #         setting.mail_text = data['mail_text']
    #         setting.headmaster_name = data['headmaster_name']
    #         setting.mail_subject = data['mail_subject']
    #         setting.phone_number = data['phone_number']
    #         setting.mail_email = data['mail_email']
    #         setting.mail_password = data['mail_password']
    #         setting.video_link = data['video_link']
    #         setting.google_key = data['google_key']

    #         setting.save()
    #         return Response(status=status.HTTP_202_ACCEPTED)