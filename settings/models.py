from django.db import models


# Create your models here.
def principal_signature_upload_path(instance, filename):
    return '/'.join(['signature', filename])


def logo_upload_path(instance, filename):
    return '/'.join(['logo', filename])


class Setting(models.Model):
    value = models.TextField(null=True)
    key = models.CharField(max_length=500)